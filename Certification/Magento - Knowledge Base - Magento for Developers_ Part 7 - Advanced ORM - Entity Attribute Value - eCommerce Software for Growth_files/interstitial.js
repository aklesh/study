var interstitialBox = {};
var maxInterstitial = 3;

if (window.MultiBox) {
    interstitialBox = new MultiBox('ib', {useOverlay:true,showNumbers:false,showControls:false,openFromLink:false,containerClass:'contactMultibox'});
    window.addEvent("domready", function() {
        runIntersitial();
    });
}

function runIntersitial(){
    if (isLoggedIn() || !isHomepage()) {
	if(getCookie('interstitial')){
	    var currCookie = parseFloat(getCookie('interstitial'));
	    if(currCookie < maxInterstitial){
		setCookie('interstitial', (currCookie+1), 365);
		interstitialPopup();
	    }
	}else{
	    setCookie('interstitial', 1, 365);
	    interstitialPopup();
	}
    }
}

function interstitialPopup(){
    interstitialBox.open($('ib1'));
}

function rejectInterstitial(){
    setCookie('interstitial', maxInterstitial, 365);
}

function hidePopupSelects(){
    $$('.popup-select dd').each(function(dd){
	dd.style.display = 'none';
    });
}

function selectInitialize(){
    var interstitialForm = $('interstitial-form');
    $$('.popup-select').each(function(sel){
	var thisList = $(sel.id + '-list');
	var thisHead = $(sel.id + '-head');
	var thisText = $(sel.id + '-text');
	var thisValue = $(sel.id + '-value');
	thisHead.addEvent('click', function(e){
	    hidePopupSelects();
	    if(thisList.hasClass('active')){
		thisList.style.display = 'none';
		thisList.removeClass('active');
	    }else{
		thisList.style.display = 'block';
		thisList.addClass('active');
	    }
	    new Event(e).stop();
	});
	$$('#' + thisList.id + ' a').each(function(lnk){
	    lnk.addEvent('click', function(e){
		hidePopupSelects();
		thisValue.value = lnk.rel;
		thisText.innerHTML = lnk.innerHTML;
		if(lnk.rel != '' && $(thisList.id+'-error')) $(thisList.id+'-error').remove();
		new Event(e).stop();
	    });
	});
    });
    
    // Submit the form
    $('popup-select-submit').addEvent('click', function(e){
	new Event(e).stop();
	var errors = 0;
	$$('.is-error').each(function(err){
	    err.remove();
	});
	$$('.sel-values').each(function(val){
	    if(val.value == ''){
		errors = 1;
		var errorList = val.id.replace('-value', '');
		new Element('dd', { 'class':'is-error', 'id':errorList+'-error' }).setText('This is required field.').injectInside($(errorList));
	    }
	});
	if(errors == 0){
	    setTimeout(function(){
		interstitialBox.close();
	    }, 500);
	    interstitialForm.send();
	    rejectInterstitial();
	}
    });
}

/**
 * Checks if current page is homepage
 */
function isHomepage(){
    return window.location.pathname == '/';
}

/**
 * Checks if user logged in (global variable should be set in main template)
 */
function isLoggedIn(){
    return window.userLoggedIn || false;
}

function setCookie(name, value, expires){
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + expires);
    var c_value=escape(value) + ((expires==null) ? "" : "; expires="+exdate.toUTCString()) + "; path=/";
    document.cookie=name + "=" + c_value;
}

function getCookie(name){
    var cook = document.cookie;
    var pos = cook.indexOf(name + '=');
    if(pos == -1){
	return null;
    } else {
	var pos2 = cook.indexOf(';', pos);
	if(pos2 == -1)
	    return unescape(cook.substring(pos + name.length + 1));
	else 
	    return unescape(cook.substring(pos + name.length + 1, pos2));
    }
}
