//MooTools, My Object Oriented Javascript Tools. Copyright (c) 2006-2007 Valerio Proietti, <http://mad4milk.net>, MIT Style License.

var MooTools={version:"1.11"};function $defined(A){return(A!=undefined);}function $type(B){if(!$defined(B)){return false;}if(B.htmlElement){return"element";
}var A=typeof B;if(A=="object"&&B.nodeName){switch(B.nodeType){case 1:return"element";case 3:return(/\S/).test(B.nodeValue)?"textnode":"whitespace";}}if(A=="object"||A=="function"){switch(B.constructor){case Array:return"array";
case RegExp:return"regexp";case Class:return"class";}if(typeof B.length=="number"){if(B.item){return"collection";}if(B.callee){return"arguments";}}}return A;
}function $merge(){var C={};for(var B=0;B<arguments.length;B++){for(var E in arguments[B]){var A=arguments[B][E];var D=C[E];if(D&&$type(A)=="object"&&$type(D)=="object"){C[E]=$merge(D,A);
}else{C[E]=A;}}}return C;}var $extend=function(){var A=arguments;if(!A[1]){A=[this,A[0]];}for(var B in A[1]){A[0][B]=A[1][B];}return A[0];};var $native=function(){for(var B=0,A=arguments.length;
B<A;B++){arguments[B].extend=function(C){for(var D in C){if(!this.prototype[D]){this.prototype[D]=C[D];}if(!this[D]){this[D]=$native.generic(D);}}};}};
$native.generic=function(A){return function(B){return this.prototype[A].apply(B,Array.prototype.slice.call(arguments,1));};};$native(Function,Array,String,Number);
function $chk(A){return !!(A||A===0);}function $pick(B,A){return $defined(B)?B:A;}function $random(B,A){return Math.floor(Math.random()*(A-B+1)+B);}function $time(){return new Date().getTime();
}function $clear(A){clearTimeout(A);clearInterval(A);return null;}var Abstract=function(A){A=A||{};A.extend=$extend;return A;};var Window=new Abstract(window);
var Document=new Abstract(document);document.head=document.getElementsByTagName("head")[0];window.xpath=!!(document.evaluate);if(window.ActiveXObject){window.ie=window[window.XMLHttpRequest?"ie7":"ie6"]=true;
}else{if(document.childNodes&&!document.all&&!navigator.taintEnabled){window.webkit=window[window.xpath?"webkit420":"webkit419"]=true;}else{if(document.getBoxObjectFor!=null){window.gecko=true;
}}}window.khtml=window.webkit;Object.extend=$extend;if(typeof HTMLElement=="undefined"){var HTMLElement=function(){};if(window.webkit){document.createElement("iframe");
}HTMLElement.prototype=(window.webkit)?window["[[DOMElement.prototype]]"]:{};}HTMLElement.prototype.htmlElement=function(){};if(window.ie6){try{document.execCommand("BackgroundImageCache",false,true);
}catch(e){}}var Class=function(B){var A=function(){return(arguments[0]!==null&&this.initialize&&$type(this.initialize)=="function")?this.initialize.apply(this,arguments):this;
};$extend(A,this);A.prototype=B;A.constructor=Class;return A;};Class.empty=function(){};Class.prototype={extend:function(B){var C=new this(null);for(var D in B){var A=C[D];
C[D]=Class.Merge(A,B[D]);}return new Class(C);},implement:function(){for(var B=0,A=arguments.length;B<A;B++){$extend(this.prototype,arguments[B]);}}};Class.Merge=function(C,D){if(C&&C!=D){var B=$type(D);
if(B!=$type(C)){return D;}switch(B){case"function":var A=function(){this.parent=arguments.callee.parent;return D.apply(this,arguments);};A.parent=C;return A;
case"object":return $merge(C,D);}}return D;};var Chain=new Class({chain:function(A){this.chains=this.chains||[];this.chains.push(A);return this;},callChain:function(){if(this.chains&&this.chains.length){this.chains.shift().delay(10,this);
}},clearChain:function(){this.chains=[];}});var Events=new Class({addEvent:function(B,A){if(A!=Class.empty){this.$events=this.$events||{};this.$events[B]=this.$events[B]||[];
this.$events[B].include(A);}return this;},fireEvent:function(C,B,A){if(this.$events&&this.$events[C]){this.$events[C].each(function(D){D.create({bind:this,delay:A,"arguments":B})();
},this);}return this;},removeEvent:function(B,A){if(this.$events&&this.$events[B]){this.$events[B].remove(A);}return this;}});var Options=new Class({setOptions:function(){this.options=$merge.apply(null,[this.options].extend(arguments));
if(this.addEvent){for(var A in this.options){if($type(this.options[A]=="function")&&(/^on[A-Z]/).test(A)){this.addEvent(A,this.options[A]);}}}return this;
}});Array.extend({forEach:function(C,D){for(var B=0,A=this.length;B<A;B++){C.call(D,this[B],B,this);}},filter:function(D,E){var C=[];for(var B=0,A=this.length;
B<A;B++){if(D.call(E,this[B],B,this)){C.push(this[B]);}}return C;},map:function(D,E){var C=[];for(var B=0,A=this.length;B<A;B++){C[B]=D.call(E,this[B],B,this);
}return C;},every:function(C,D){for(var B=0,A=this.length;B<A;B++){if(!C.call(D,this[B],B,this)){return false;}}return true;},some:function(C,D){for(var B=0,A=this.length;
B<A;B++){if(C.call(D,this[B],B,this)){return true;}}return false;},indexOf:function(C,D){var A=this.length;for(var B=(D<0)?Math.max(0,A+D):D||0;B<A;B++){if(this[B]===C){return B;
}}return -1;},copy:function(D,C){D=D||0;if(D<0){D=this.length+D;}C=C||(this.length-D);var A=[];for(var B=0;B<C;B++){A[B]=this[D++];}return A;},remove:function(C){var B=0;
var A=this.length;while(B<A){if(this[B]===C){this.splice(B,1);A--;}else{B++;}}return this;},contains:function(A,B){return this.indexOf(A,B)!=-1;},associate:function(C){var D={},B=Math.min(this.length,C.length);
for(var A=0;A<B;A++){D[C[A]]=this[A];}return D;},extend:function(C){for(var B=0,A=C.length;B<A;B++){this.push(C[B]);}return this;},merge:function(C){for(var B=0,A=C.length;
B<A;B++){this.include(C[B]);}return this;},include:function(A){if(!this.contains(A)){this.push(A);}return this;},getRandom:function(){return this[$random(0,this.length-1)]||null;
},getLast:function(){return this[this.length-1]||null;}});Array.prototype.each=Array.prototype.forEach;Array.each=Array.forEach;function $A(A){return Array.copy(A);
}function $each(C,B,D){if(C&&typeof C.length=="number"&&$type(C)!="object"){Array.forEach(C,B,D);}else{for(var A in C){B.call(D||C,C[A],A);}}}Array.prototype.test=Array.prototype.contains;
String.extend({test:function(A,B){return(($type(A)=="string")?new RegExp(A,B):A).test(this);},toInt:function(){return parseInt(this,10);},toFloat:function(){return parseFloat(this);
},camelCase:function(){return this.replace(/-\D/g,function(A){return A.charAt(1).toUpperCase();});},hyphenate:function(){return this.replace(/\w[A-Z]/g,function(A){return(A.charAt(0)+"-"+A.charAt(1).toLowerCase());
});},capitalize:function(){return this.replace(/\b[a-z]/g,function(A){return A.toUpperCase();});},trim:function(){return this.replace(/^\s+|\s+$/g,"");
},clean:function(){return this.replace(/\s{2,}/g," ").trim();},rgbToHex:function(B){var A=this.match(/\d{1,3}/g);return(A)?A.rgbToHex(B):false;},hexToRgb:function(B){var A=this.match(/^#?(\w{1,2})(\w{1,2})(\w{1,2})$/);
return(A)?A.slice(1).hexToRgb(B):false;},contains:function(A,B){return(B)?(B+this+B).indexOf(B+A+B)>-1:this.indexOf(A)>-1;},escapeRegExp:function(){return this.replace(/([.*+?^${}()|[\]\/\\])/g,"\\$1");
}});Array.extend({rgbToHex:function(D){if(this.length<3){return false;}if(this.length==4&&this[3]==0&&!D){return"transparent";}var B=[];for(var A=0;A<3;
A++){var C=(this[A]-0).toString(16);B.push((C.length==1)?"0"+C:C);}return D?B:"#"+B.join("");},hexToRgb:function(C){if(this.length!=3){return false;}var A=[];
for(var B=0;B<3;B++){A.push(parseInt((this[B].length==1)?this[B]+this[B]:this[B],16));}return C?A:"rgb("+A.join(",")+")";}});Function.extend({create:function(A){var B=this;
A=$merge({bind:B,event:false,"arguments":null,delay:false,periodical:false,attempt:false},A);if($chk(A.arguments)&&$type(A.arguments)!="array"){A.arguments=[A.arguments];
}return function(E){var C;if(A.event){E=E||window.event;C=[(A.event===true)?E:new A.event(E)];if(A.arguments){C.extend(A.arguments);}}else{C=A.arguments||arguments;
}var F=function(){return B.apply($pick(A.bind,B),C);};if(A.delay){return setTimeout(F,A.delay);}if(A.periodical){return setInterval(F,A.periodical);}if(A.attempt){try{return F();
}catch(D){return false;}}return F();};},pass:function(A,B){return this.create({"arguments":A,bind:B});},attempt:function(A,B){return this.create({"arguments":A,bind:B,attempt:true})();
},bind:function(B,A){return this.create({bind:B,"arguments":A});},bindAsEventListener:function(B,A){return this.create({bind:B,event:true,"arguments":A});
},delay:function(B,C,A){return this.create({delay:B,bind:C,"arguments":A})();},periodical:function(A,C,B){return this.create({periodical:A,bind:C,"arguments":B})();
}});Number.extend({toInt:function(){return parseInt(this);},toFloat:function(){return parseFloat(this);},limit:function(B,A){return Math.min(A,Math.max(B,this));
},round:function(A){A=Math.pow(10,A||0);return Math.round(this*A)/A;},times:function(B){for(var A=0;A<this;A++){B(A);}}});var Element=new Class({initialize:function(D,C){if($type(D)=="string"){if(window.ie&&C&&(C.name||C.type)){var A=(C.name)?' name="'+C.name+'"':"";
var B=(C.type)?' type="'+C.type+'"':"";delete C.name;delete C.type;D="<"+D+A+B+">";}D=document.createElement(D);}D=$(D);return(!C||!D)?D:D.set(C);}});var Elements=new Class({initialize:function(A){return(A)?$extend(A,this):this;
}});Elements.extend=function(A){for(var B in A){this.prototype[B]=A[B];this[B]=$native.generic(B);}};function $(B){if(!B){return null;}if(B.htmlElement){return Garbage.collect(B);
}if([window,document].contains(B)){return B;}var A=$type(B);if(A=="string"){B=document.getElementById(B);A=(B)?"element":false;}if(A!="element"){return null;
}if(B.htmlElement){return Garbage.collect(B);}if(["object","embed"].contains(B.tagName.toLowerCase())){return B;}$extend(B,Element.prototype);B.htmlElement=function(){};
return Garbage.collect(B);}document.getElementsBySelector=document.getElementsByTagName;function $$(){var D=[];for(var C=0,B=arguments.length;C<B;C++){var A=arguments[C];
switch($type(A)){case"element":D.push(A);case"boolean":break;case false:break;case"string":A=document.getElementsBySelector(A,true);default:D.extend(A);
}}return $$.unique(D);}$$.unique=function(G){var D=[];for(var C=0,A=G.length;C<A;C++){if(G[C].$included){continue;}var B=$(G[C]);if(B&&!B.$included){B.$included=true;
D.push(B);}}for(var F=0,E=D.length;F<E;F++){D[F].$included=null;}return new Elements(D);};Elements.Multi=function(A){return function(){var D=arguments;
var B=[];var G=true;for(var E=0,C=this.length,F;E<C;E++){F=this[E][A].apply(this[E],D);if($type(F)!="element"){G=false;}B.push(F);}return(G)?$$.unique(B):B;
};};Element.extend=function(A){for(var B in A){HTMLElement.prototype[B]=A[B];Element.prototype[B]=A[B];Element[B]=$native.generic(B);var C=(Array.prototype[B])?B+"Elements":B;
Elements.prototype[C]=Elements.Multi(B);}};Element.extend({set:function(A){for(var C in A){var B=A[C];switch(C){case"styles":this.setStyles(B);break;case"events":if(this.addEvents){this.addEvents(B);
}break;case"properties":this.setProperties(B);break;default:this.setProperty(C,B);}}return this;},inject:function(C,A){C=$(C);switch(A){case"before":C.parentNode.insertBefore(this,C);
break;case"after":var B=C.getNext();if(!B){C.parentNode.appendChild(this);}else{C.parentNode.insertBefore(this,B);}break;case"top":var D=C.firstChild;if(D){C.insertBefore(this,D);
break;}default:C.appendChild(this);}return this;},injectBefore:function(A){return this.inject(A,"before");},injectAfter:function(A){return this.inject(A,"after");
},injectInside:function(A){return this.inject(A,"bottom");},injectTop:function(A){return this.inject(A,"top");},adopt:function(){var A=[];$each(arguments,function(B){A=A.concat(B);
});$$(A).inject(this);return this;},remove:function(){return this.parentNode.removeChild(this);},clone:function(C){var B=$(this.cloneNode(C!==false));if(!B.$events){return B;
}B.$events={};for(var A in this.$events){B.$events[A]={keys:$A(this.$events[A].keys),values:$A(this.$events[A].values)};}return B.removeEvents();},replaceWith:function(A){A=$(A);
this.parentNode.replaceChild(A,this);return A;},appendText:function(A){this.appendChild(document.createTextNode(A));return this;},hasClass:function(A){return this.className.contains(A," ");
},addClass:function(A){if(!this.hasClass(A)){this.className=(this.className+" "+A).clean();}return this;},removeClass:function(A){this.className=this.className.replace(new RegExp("(^|\\s)"+A+"(?:\\s|$)"),"$1").clean();
return this;},toggleClass:function(A){return this.hasClass(A)?this.removeClass(A):this.addClass(A);},setStyle:function(B,A){switch(B){case"opacity":return this.setOpacity(parseFloat(A));
case"float":B=(window.ie)?"styleFloat":"cssFloat";}B=B.camelCase();switch($type(A)){case"number":if(!["zIndex","zoom"].contains(B)){A+="px";}break;case"array":A="rgb("+A.join(",")+")";
}this.style[B]=A;return this;},setStyles:function(A){switch($type(A)){case"object":Element.setMany(this,"setStyle",A);break;case"string":this.style.cssText=A;
}return this;},setOpacity:function(A){if(A==0){if(this.style.visibility!="hidden"){this.style.visibility="hidden";}}else{if(this.style.visibility!="visible"){this.style.visibility="visible";
}}if(!this.currentStyle||!this.currentStyle.hasLayout){this.style.zoom=1;}if(window.ie){this.style.filter=(A==1)?"":"alpha(opacity="+A*100+")";}this.style.opacity=this.$tmp.opacity=A;
return this;},getStyle:function(C){C=C.camelCase();var A=this.style[C];if(!$chk(A)){if(C=="opacity"){return this.$tmp.opacity;}A=[];for(var B in Element.Styles){if(C==B){Element.Styles[B].each(function(F){var E=this.getStyle(F);
A.push(parseInt(E)?E:"0px");},this);if(C=="border"){var D=A.every(function(E){return(E==A[0]);});return(D)?A[0]:false;}return A.join(" ");}}if(C.contains("border")){if(Element.Styles.border.contains(C)){return["Width","Style","Color"].map(function(E){return this.getStyle(C+E);
},this).join(" ");}else{if(Element.borderShort.contains(C)){return["Top","Right","Bottom","Left"].map(function(E){return this.getStyle("border"+E+C.replace("border",""));
},this).join(" ");}}}if(document.defaultView){A=document.defaultView.getComputedStyle(this,null).getPropertyValue(C.hyphenate());}else{if(this.currentStyle){A=this.currentStyle[C];
}}}if(window.ie){A=Element.fixStyle(C,A,this);}if(A&&C.test(/color/i)&&A.contains("rgb")){return A.split("rgb").splice(1,4).map(function(E){return E.rgbToHex();
}).join(" ");}return A;},getStyles:function(){return Element.getMany(this,"getStyle",arguments);},walk:function(A,C){A+="Sibling";var B=(C)?this[C]:this[A];
while(B&&$type(B)!="element"){B=B[A];}return $(B);},getPrevious:function(){return this.walk("previous");},getNext:function(){return this.walk("next");},getFirst:function(){return this.walk("next","firstChild");
},getLast:function(){return this.walk("previous","lastChild");},getParent:function(){return $(this.parentNode);},getChildren:function(){return $$(this.childNodes);
},hasChild:function(A){return !!$A(this.getElementsByTagName("*")).contains(A);},getProperty:function(D){var B=Element.Properties[D];if(B){return this[B];
}var A=Element.PropertiesIFlag[D]||0;if(!window.ie||A){return this.getAttribute(D,A);}var C=this.attributes[D];return(C)?C.nodeValue:null;},removeProperty:function(B){var A=Element.Properties[B];
if(A){this[A]="";}else{this.removeAttribute(B);}return this;},getProperties:function(){return Element.getMany(this,"getProperty",arguments);},setProperty:function(C,B){var A=Element.Properties[C];
if(A){this[A]=B;}else{this.setAttribute(C,B);}return this;},setProperties:function(A){return Element.setMany(this,"setProperty",A);},setHTML:function(){this.innerHTML=$A(arguments).join("");
return this;},setText:function(B){var A=this.getTag();if(["style","script"].contains(A)){if(window.ie){if(A=="style"){this.styleSheet.cssText=B;}else{if(A=="script"){this.setProperty("text",B);
}}return this;}else{this.removeChild(this.firstChild);return this.appendText(B);}}this[$defined(this.innerText)?"innerText":"textContent"]=B;return this;
},getText:function(){var A=this.getTag();if(["style","script"].contains(A)){if(window.ie){if(A=="style"){return this.styleSheet.cssText;}else{if(A=="script"){return this.getProperty("text");
}}}else{return this.innerHTML;}}return($pick(this.innerText,this.textContent));},getTag:function(){return this.tagName.toLowerCase();},empty:function(){Garbage.trash(this.getElementsByTagName("*"));
return this.setHTML("");}});Element.fixStyle=function(E,A,D){if($chk(parseInt(A))){return A;}if(["height","width"].contains(E)){var B=(E=="width")?["left","right"]:["top","bottom"];
var C=0;B.each(function(F){C+=D.getStyle("border-"+F+"-width").toInt()+D.getStyle("padding-"+F).toInt();});return D["offset"+E.capitalize()]-C+"px";}else{if(E.test(/border(.+)Width|margin|padding/)){return"0px";
}}return A;};Element.Styles={border:[],padding:[],margin:[]};["Top","Right","Bottom","Left"].each(function(B){for(var A in Element.Styles){Element.Styles[A].push(A+B);
}});Element.borderShort=["borderWidth","borderStyle","borderColor"];Element.getMany=function(B,D,C){var A={};$each(C,function(E){A[E]=B[D](E);});return A;
};Element.setMany=function(B,D,C){for(var A in C){B[D](A,C[A]);}return B;};Element.Properties=new Abstract({"class":"className","for":"htmlFor",colspan:"colSpan",rowspan:"rowSpan",accesskey:"accessKey",tabindex:"tabIndex",maxlength:"maxLength",readonly:"readOnly",frameborder:"frameBorder",value:"value",disabled:"disabled",checked:"checked",multiple:"multiple",selected:"selected"});
Element.PropertiesIFlag={href:2,src:2};Element.Methods={Listeners:{addListener:function(B,A){if(this.addEventListener){this.addEventListener(B,A,false);
}else{this.attachEvent("on"+B,A);}return this;},removeListener:function(B,A){if(this.removeEventListener){this.removeEventListener(B,A,false);}else{this.detachEvent("on"+B,A);
}return this;}}};window.extend(Element.Methods.Listeners);document.extend(Element.Methods.Listeners);Element.extend(Element.Methods.Listeners);var Garbage={elements:[],collect:function(A){if(!A.$tmp){Garbage.elements.push(A);
A.$tmp={opacity:1};}return A;},trash:function(D){for(var B=0,A=D.length,C;B<A;B++){if(!(C=D[B])||!C.$tmp){continue;}if(C.$events){C.fireEvent("trash").removeEvents();
}for(var E in C.$tmp){C.$tmp[E]=null;}for(var F in Element.prototype){C[F]=null;}Garbage.elements[Garbage.elements.indexOf(C)]=null;C.htmlElement=C.$tmp=C=null;
}Garbage.elements.remove(null);},empty:function(){Garbage.collect(window);Garbage.collect(document);Garbage.trash(Garbage.elements);}};window.addListener("beforeunload",function(){window.addListener("unload",Garbage.empty);
if(window.ie){window.addListener("unload",CollectGarbage);}});var Event=new Class({initialize:function(C){if(C&&C.$extended){return C;}this.$extended=true;
C=C||window.event;this.event=C;this.type=C.type;this.target=C.target||C.srcElement;if(this.target.nodeType==3){this.target=this.target.parentNode;}this.shift=C.shiftKey;
this.control=C.ctrlKey;this.alt=C.altKey;this.meta=C.metaKey;if(["DOMMouseScroll","mousewheel"].contains(this.type)){this.wheel=(C.wheelDelta)?C.wheelDelta/120:-(C.detail||0)/3;
}else{if(this.type.contains("key")){this.code=C.which||C.keyCode;for(var B in Event.keys){if(Event.keys[B]==this.code){this.key=B;break;}}if(this.type=="keydown"){var A=this.code-111;
if(A>0&&A<13){this.key="f"+A;}}this.key=this.key||String.fromCharCode(this.code).toLowerCase();}else{if(this.type.test(/(click|mouse|menu)/)){this.page={x:C.pageX||C.clientX+document.documentElement.scrollLeft,y:C.pageY||C.clientY+document.documentElement.scrollTop};
this.client={x:C.pageX?C.pageX-window.pageXOffset:C.clientX,y:C.pageY?C.pageY-window.pageYOffset:C.clientY};this.rightClick=(C.which==3)||(C.button==2);
switch(this.type){case"mouseover":this.relatedTarget=C.relatedTarget||C.fromElement;break;case"mouseout":this.relatedTarget=C.relatedTarget||C.toElement;
}this.fixRelatedTarget();}}}return this;},stop:function(){return this.stopPropagation().preventDefault();},stopPropagation:function(){if(this.event.stopPropagation){this.event.stopPropagation();
}else{this.event.cancelBubble=true;}return this;},preventDefault:function(){if(this.event.preventDefault){this.event.preventDefault();}else{this.event.returnValue=false;
}return this;}});Event.fix={relatedTarget:function(){if(this.relatedTarget&&this.relatedTarget.nodeType==3){this.relatedTarget=this.relatedTarget.parentNode;
}},relatedTargetGecko:function(){try{Event.fix.relatedTarget.call(this);}catch(A){this.relatedTarget=this.target;}}};Event.prototype.fixRelatedTarget=(window.gecko)?Event.fix.relatedTargetGecko:Event.fix.relatedTarget;
Event.keys=new Abstract({enter:13,up:38,down:40,left:37,right:39,esc:27,space:32,backspace:8,tab:9,"delete":46});Element.Methods.Events={addEvent:function(C,B){this.$events=this.$events||{};
this.$events[C]=this.$events[C]||{keys:[],values:[]};if(this.$events[C].keys.contains(B)){return this;}this.$events[C].keys.push(B);var A=C;var D=Element.Events[C];
if(D){if(D.add){D.add.call(this,B);}if(D.map){B=D.map;}if(D.type){A=D.type;}}if(!this.addEventListener){B=B.create({bind:this,event:true});}this.$events[C].values.push(B);
return(Element.NativeEvents.contains(A))?this.addListener(A,B):this;},removeEvent:function(C,B){if(!this.$events||!this.$events[C]){return this;}var F=this.$events[C].keys.indexOf(B);
if(F==-1){return this;}var A=this.$events[C].keys.splice(F,1)[0];var E=this.$events[C].values.splice(F,1)[0];var D=Element.Events[C];if(D){if(D.remove){D.remove.call(this,B);
}if(D.type){C=D.type;}}return(Element.NativeEvents.contains(C))?this.removeListener(C,E):this;},addEvents:function(A){return Element.setMany(this,"addEvent",A);
},removeEvents:function(A){if(!this.$events){return this;}if(!A){for(var B in this.$events){this.removeEvents(B);}this.$events=null;}else{if(this.$events[A]){this.$events[A].keys.each(function(C){this.removeEvent(A,C);
},this);this.$events[A]=null;}}return this;},fireEvent:function(C,B,A){if(this.$events&&this.$events[C]){this.$events[C].keys.each(function(D){D.create({bind:this,delay:A,"arguments":B})();
},this);}return this;},cloneEvents:function(C,A){if(!C.$events){return this;}if(!A){for(var B in C.$events){this.cloneEvents(C,B);}}else{if(C.$events[A]){C.$events[A].keys.each(function(D){this.addEvent(A,D);
},this);}}return this;}};window.extend(Element.Methods.Events);document.extend(Element.Methods.Events);Element.extend(Element.Methods.Events);Element.Events=new Abstract({mouseenter:{type:"mouseover",map:function(A){A=new Event(A);
if(A.relatedTarget!=this&&!this.hasChild(A.relatedTarget)){this.fireEvent("mouseenter",A);}}},mouseleave:{type:"mouseout",map:function(A){A=new Event(A);
if(A.relatedTarget!=this&&!this.hasChild(A.relatedTarget)){this.fireEvent("mouseleave",A);}}},mousewheel:{type:(window.gecko)?"DOMMouseScroll":"mousewheel"}});
Element.NativeEvents=["click","dblclick","mouseup","mousedown","mousewheel","DOMMouseScroll","mouseover","mouseout","mousemove","keydown","keypress","keyup","load","unload","beforeunload","resize","move","focus","blur","change","submit","reset","select","error","abort","contextmenu","scroll"];
Function.extend({bindWithEvent:function(B,A){return this.create({bind:B,"arguments":A,event:Event});}});Elements.extend({filterByTag:function(A){return new Elements(this.filter(function(B){return(Element.getTag(B)==A);
}));},filterByClass:function(A,C){var B=this.filter(function(D){return(D.className&&D.className.contains(A," "));});return(C)?B:new Elements(B);},filterById:function(C,B){var A=this.filter(function(D){return(D.id==C);
});return(B)?A:new Elements(A);},filterByAttribute:function(B,A,D,E){var C=this.filter(function(F){var G=Element.getProperty(F,B);if(!G){return false;}if(!A){return true;
}switch(A){case"=":return(G==D);case"*=":return(G.contains(D));case"^=":return(G.substr(0,D.length)==D);case"$=":return(G.substr(G.length-D.length)==D);
case"!=":return(G!=D);case"~=":return G.contains(D," ");}return false;});return(E)?C:new Elements(C);}});function $E(A,B){return($(B)||document).getElement(A);
}function $ES(A,B){return($(B)||document).getElementsBySelector(A);}$$.shared={regexp:/^(\w*|\*)(?:#([\w-]+)|\.([\w-]+))?(?:\[(\w+)(?:([!*^$]?=)["']?([^"'\]]*)["']?)?])?$/,xpath:{getParam:function(B,D,E,C){var A=[D.namespaceURI?"xhtml:":"",E[1]];
if(E[2]){A.push('[@id="',E[2],'"]');}if(E[3]){A.push('[contains(concat(" ", @class, " "), " ',E[3],' ")]');}if(E[4]){if(E[5]&&E[6]){switch(E[5]){case"*=":A.push("[contains(@",E[4],', "',E[6],'")]');
break;case"^=":A.push("[starts-with(@",E[4],', "',E[6],'")]');break;case"$=":A.push("[substring(@",E[4],", string-length(@",E[4],") - ",E[6].length,' + 1) = "',E[6],'"]');
break;case"=":A.push("[@",E[4],'="',E[6],'"]');break;case"!=":A.push("[@",E[4],'!="',E[6],'"]');}}else{A.push("[@",E[4],"]");}}B.push(A.join(""));return B;
},getItems:function(B,E,G){var F=[];var A=document.evaluate(".//"+B.join("//"),E,$$.shared.resolver,XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,null);for(var D=0,C=A.snapshotLength;
D<C;D++){F.push(A.snapshotItem(D));}return(G)?F:new Elements(F.map($));}},normal:{getParam:function(A,C,E,B){if(B==0){if(E[2]){var D=C.getElementById(E[2]);
if(!D||((E[1]!="*")&&(Element.getTag(D)!=E[1]))){return false;}A=[D];}else{A=$A(C.getElementsByTagName(E[1]));}}else{A=$$.shared.getElementsByTagName(A,E[1]);
if(E[2]){A=Elements.filterById(A,E[2],true);}}if(E[3]){A=Elements.filterByClass(A,E[3],true);}if(E[4]){A=Elements.filterByAttribute(A,E[4],E[5],E[6],true);
}return A;},getItems:function(A,B,C){return(C)?A:$$.unique(A);}},resolver:function(A){return(A=="xhtml")?"http://www.w3.org/1999/xhtml":false;},getElementsByTagName:function(D,C){var E=[];
for(var B=0,A=D.length;B<A;B++){E.extend(D[B].getElementsByTagName(C));}return E;}};$$.shared.method=(window.xpath)?"xpath":"normal";Element.Methods.Dom={getElements:function(A,H){var C=[];
A=A.trim().split(" ");for(var E=0,D=A.length;E<D;E++){var F=A[E];var G=F.match($$.shared.regexp);if(!G){break;}G[1]=G[1]||"*";var B=$$.shared[$$.shared.method].getParam(C,this,G,E);
if(!B){break;}C=B;}return $$.shared[$$.shared.method].getItems(C,this,H);},getElement:function(A){return $(this.getElements(A,true)[0]||false);},getElementsBySelector:function(A,E){var D=[];
A=A.split(",");for(var C=0,B=A.length;C<B;C++){D=D.concat(this.getElements(A[C],true));}return(E)?D:$$.unique(D);}};Element.extend({getElementById:function(C){var B=document.getElementById(C);
if(!B){return false;}for(var A=B.parentNode;A!=this;A=A.parentNode){if(!A){return false;}}return B;},getElementsByClassName:function(A){return this.getElements("."+A);
}});document.extend(Element.Methods.Dom);Element.extend(Element.Methods.Dom);Element.extend({getValue:function(){switch(this.getTag()){case"select":var A=[];
$each(this.options,function(B){if(B.selected){A.push($pick(B.value,B.text));}});return(this.multiple)?A:A[0];case"input":if(!(this.checked&&["checkbox","radio"].contains(this.type))&&!["hidden","text","password"].contains(this.type)){break;
}case"textarea":return this.value;}return false;},getFormElements:function(){return $$(this.getElementsByTagName("input"),this.getElementsByTagName("select"),this.getElementsByTagName("textarea"));
},toQueryString:function(){var A=[];this.getFormElements().each(function(D){var C=D.name;var E=D.getValue();if(E===false||!C||D.disabled){return ;}var B=function(F){A.push(C+"="+encodeURIComponent(F));
};if($type(E)=="array"){E.each(B);}else{B(E);}});return A.join("&");}});Element.extend({scrollTo:function(A,B){this.scrollLeft=A;this.scrollTop=B;},getSize:function(){return{scroll:{x:this.scrollLeft,y:this.scrollTop},size:{x:this.offsetWidth,y:this.offsetHeight},scrollSize:{x:this.scrollWidth,y:this.scrollHeight}};
},getPosition:function(A){A=A||[];var B=this,D=0,C=0;do{D+=B.offsetLeft||0;C+=B.offsetTop||0;B=B.offsetParent;}while(B);A.each(function(E){D-=E.scrollLeft||0;
C-=E.scrollTop||0;});return{x:D,y:C};},getTop:function(A){return this.getPosition(A).y;},getLeft:function(A){return this.getPosition(A).x;},getCoordinates:function(B){var A=this.getPosition(B);
var C={width:this.offsetWidth,height:this.offsetHeight,left:A.x,top:A.y};C.right=C.left+C.width;C.bottom=C.top+C.height;return C;}});Element.Events.domready={add:function(B){if(window.loaded){B.call(this);
return ;}var A=function(){if(window.loaded){return ;}window.loaded=true;window.timer=$clear(window.timer);this.fireEvent("domready");}.bind(this);if(document.readyState&&window.webkit){window.timer=function(){if(["loaded","complete"].contains(document.readyState)){A();
}}.periodical(50);}else{if(document.readyState&&window.ie){if(!$("ie_ready")){var C=(window.location.protocol=="https:")?"://0":"javascript:void(0)";document.write('<script id="ie_ready" defer src="'+C+'"><\/script>');
$("ie_ready").onreadystatechange=function(){if(this.readyState=="complete"){A();}};}}else{window.addListener("load",A);document.addListener("DOMContentLoaded",A);
}}}};window.onDomReady=function(A){return this.addEvent("domready",A);};window.extend({getWidth:function(){if(this.webkit419){return this.innerWidth;}if(this.opera){return document.body.clientWidth;
}return document.documentElement.clientWidth;},getHeight:function(){if(this.webkit419){return this.innerHeight;}if(this.opera){return document.body.clientHeight;
}return document.documentElement.clientHeight;},getScrollWidth:function(){if(this.ie){return Math.max(document.documentElement.offsetWidth,document.documentElement.scrollWidth);
}if(this.webkit){return document.body.scrollWidth;}return document.documentElement.scrollWidth;},getScrollHeight:function(){if(this.ie){return Math.max(document.documentElement.offsetHeight,document.documentElement.scrollHeight);
}if(this.webkit){return document.body.scrollHeight;}return document.documentElement.scrollHeight;},getScrollLeft:function(){return this.pageXOffset||document.documentElement.scrollLeft;
},getScrollTop:function(){return this.pageYOffset||document.documentElement.scrollTop;},getSize:function(){return{size:{x:this.getWidth(),y:this.getHeight()},scrollSize:{x:this.getScrollWidth(),y:this.getScrollHeight()},scroll:{x:this.getScrollLeft(),y:this.getScrollTop()}};
},getPosition:function(){return{x:0,y:0};}});var Fx={};Fx.Base=new Class({options:{onStart:Class.empty,onComplete:Class.empty,onCancel:Class.empty,transition:function(A){return -(Math.cos(Math.PI*A)-1)/2;
},duration:500,unit:"px",wait:true,fps:50},initialize:function(A){this.element=this.element||null;this.setOptions(A);if(this.options.initialize){this.options.initialize.call(this);
}},step:function(){var A=$time();if(A<this.time+this.options.duration){this.delta=this.options.transition((A-this.time)/this.options.duration);this.setNow();
this.increase();}else{this.stop(true);this.set(this.to);this.fireEvent("onComplete",this.element,10);this.callChain();}},set:function(A){this.now=A;this.increase();
return this;},setNow:function(){this.now=this.compute(this.from,this.to);},compute:function(B,A){return(A-B)*this.delta+B;},start:function(B,A){if(!this.options.wait){this.stop();
}else{if(this.timer){return this;}}this.from=B;this.to=A;this.change=this.to-this.from;this.time=$time();this.timer=this.step.periodical(Math.round(1000/this.options.fps),this);
this.fireEvent("onStart",this.element);return this;},stop:function(A){if(!this.timer){return this;}this.timer=$clear(this.timer);if(!A){this.fireEvent("onCancel",this.element);
}return this;},custom:function(B,A){return this.start(B,A);},clearTimer:function(A){return this.stop(A);}});Fx.Base.implement(new Chain,new Events,new Options);
Fx.CSS={select:function(B,C){if(B.test(/color/i)){return this.Color;}var A=$type(C);if((A=="array")||(A=="string"&&C.contains(" "))){return this.Multi;
}return this.Single;},parse:function(C,D,A){if(!A.push){A=[A];}var F=A[0],E=A[1];if(!$chk(E)){E=F;F=C.getStyle(D);}var B=this.select(D,E);return{from:B.parse(F),to:B.parse(E),css:B};
}};Fx.CSS.Single={parse:function(A){return parseFloat(A);},getNow:function(C,B,A){return A.compute(C,B);},getValue:function(C,A,B){if(A=="px"&&B!="opacity"){C=Math.round(C);
}return C+A;}};Fx.CSS.Multi={parse:function(A){return A.push?A:A.split(" ").map(function(B){return parseFloat(B);});},getNow:function(E,D,C){var A=[];for(var B=0;
B<E.length;B++){A[B]=C.compute(E[B],D[B]);}return A;},getValue:function(C,A,B){if(A=="px"&&B!="opacity"){C=C.map(Math.round);}return C.join(A+" ")+A;}};
Fx.CSS.Color={parse:function(A){return A.push?A:A.hexToRgb(true);},getNow:function(E,D,C){var A=[];for(var B=0;B<E.length;B++){A[B]=Math.round(C.compute(E[B],D[B]));
}return A;},getValue:function(A){return"rgb("+A.join(",")+")";}};Fx.Style=Fx.Base.extend({initialize:function(B,C,A){this.element=$(B);this.property=C;
this.parent(A);},hide:function(){return this.set(0);},setNow:function(){this.now=this.css.getNow(this.from,this.to,this);},set:function(A){this.css=Fx.CSS.select(this.property,A);
return this.parent(this.css.parse(A));},start:function(C,B){if(this.timer&&this.options.wait){return this;}var A=Fx.CSS.parse(this.element,this.property,[C,B]);
this.css=A.css;return this.parent(A.from,A.to);},increase:function(){this.element.setStyle(this.property,this.css.getValue(this.now,this.options.unit,this.property));
}});Element.extend({effect:function(B,A){return new Fx.Style(this,B,A);}});Fx.Styles=Fx.Base.extend({initialize:function(B,A){this.element=$(B);this.parent(A);
},setNow:function(){for(var A in this.from){this.now[A]=this.css[A].getNow(this.from[A],this.to[A],this);}},set:function(C){var A={};this.css={};for(var B in C){this.css[B]=Fx.CSS.select(B,C[B]);
A[B]=this.css[B].parse(C[B]);}return this.parent(A);},start:function(C){if(this.timer&&this.options.wait){return this;}this.now={};this.css={};var E={},D={};
for(var B in C){var A=Fx.CSS.parse(this.element,B,C[B]);E[B]=A.from;D[B]=A.to;this.css[B]=A.css;}return this.parent(E,D);},increase:function(){for(var A in this.now){this.element.setStyle(A,this.css[A].getValue(this.now[A],this.options.unit,A));
}}});Element.extend({effects:function(A){return new Fx.Styles(this,A);}});Fx.Elements=Fx.Base.extend({initialize:function(B,A){this.elements=$$(B);this.parent(A);
},setNow:function(){for(var C in this.from){var F=this.from[C],E=this.to[C],B=this.css[C],A=this.now[C]={};for(var D in F){A[D]=B[D].getNow(F[D],E[D],this);
}}},set:function(G){var B={};this.css={};for(var D in G){var F=G[D],C=this.css[D]={},A=B[D]={};for(var E in F){C[E]=Fx.CSS.select(E,F[E]);A[E]=C[E].parse(F[E]);
}}return this.parent(B);},start:function(D){if(this.timer&&this.options.wait){return this;}this.now={};this.css={};var I={},J={};for(var E in D){var G=D[E],A=I[E]={},H=J[E]={},C=this.css[E]={};
for(var B in G){var F=Fx.CSS.parse(this.elements[E],B,G[B]);A[B]=F.from;H[B]=F.to;C[B]=F.css;}}return this.parent(I,J);},increase:function(){for(var C in this.now){var A=this.now[C],B=this.css[C];
for(var D in A){this.elements[C].setStyle(D,B[D].getValue(A[D],this.options.unit,D));}}}});Fx.Scroll=Fx.Base.extend({options:{overflown:[],offset:{x:0,y:0},wheelStops:true},initialize:function(B,A){this.now=[];
this.element=$(B);this.bound={stop:this.stop.bind(this,false)};this.parent(A);if(this.options.wheelStops){this.addEvent("onStart",function(){document.addEvent("mousewheel",this.bound.stop);
}.bind(this));this.addEvent("onComplete",function(){document.removeEvent("mousewheel",this.bound.stop);}.bind(this));}},setNow:function(){for(var A=0;A<2;
A++){this.now[A]=this.compute(this.from[A],this.to[A]);}},scrollTo:function(B,F){if(this.timer&&this.options.wait){return this;}var D=this.element.getSize();
var C={x:B,y:F};for(var E in D.size){var A=D.scrollSize[E]-D.size[E];if($chk(C[E])){C[E]=($type(C[E])=="number")?C[E].limit(0,A):A;}else{C[E]=D.scroll[E];
}C[E]+=this.options.offset[E];}return this.start([D.scroll.x,D.scroll.y],[C.x,C.y]);},toTop:function(){return this.scrollTo(false,0);},toBottom:function(){return this.scrollTo(false,"full");
},toLeft:function(){return this.scrollTo(0,false);},toRight:function(){return this.scrollTo("full",false);},toElement:function(B){var A=this.element.getPosition(this.options.overflown);
var C=$(B).getPosition(this.options.overflown);return this.scrollTo(C.x-A.x,C.y-A.y);},increase:function(){this.element.scrollTo(this.now[0],this.now[1]);
}});Fx.Slide=Fx.Base.extend({options:{mode:"vertical"},initialize:function(B,A){this.element=$(B);this.wrapper=new Element("div",{styles:$extend(this.element.getStyles("margin"),{overflow:"hidden"})}).injectAfter(this.element).adopt(this.element);
this.element.setStyle("margin",0);this.setOptions(A);this.now=[];this.parent(this.options);this.open=true;this.addEvent("onComplete",function(){this.open=(this.now[0]===0);
});if(window.webkit419){this.addEvent("onComplete",function(){if(this.open){this.element.remove().inject(this.wrapper);}});}},setNow:function(){for(var A=0;
A<2;A++){this.now[A]=this.compute(this.from[A],this.to[A]);}},vertical:function(){this.margin="margin-top";this.layout="height";this.offset=this.element.offsetHeight;
},horizontal:function(){this.margin="margin-left";this.layout="width";this.offset=this.element.offsetWidth;},slideIn:function(A){this[A||this.options.mode]();
return this.start([this.element.getStyle(this.margin).toInt(),this.wrapper.getStyle(this.layout).toInt()],[0,this.offset]);},slideOut:function(A){this[A||this.options.mode]();
return this.start([this.element.getStyle(this.margin).toInt(),this.wrapper.getStyle(this.layout).toInt()],[-this.offset,0]);},hide:function(A){this[A||this.options.mode]();
this.open=false;return this.set([-this.offset,0]);},show:function(A){this[A||this.options.mode]();this.open=true;return this.set([0,this.offset]);},toggle:function(A){if(this.wrapper.offsetHeight==0||this.wrapper.offsetWidth==0){return this.slideIn(A);
}return this.slideOut(A);},increase:function(){this.element.setStyle(this.margin,this.now[0]+this.options.unit);this.wrapper.setStyle(this.layout,this.now[1]+this.options.unit);
}});Fx.Transition=function(B,A){A=A||[];if($type(A)!="array"){A=[A];}return $extend(B,{easeIn:function(C){return B(C,A);},easeOut:function(C){return 1-B(1-C,A);
},easeInOut:function(C){return(C<=0.5)?B(2*C,A)/2:(2-B(2*(1-C),A))/2;}});};Fx.Transitions=new Abstract({linear:function(A){return A;}});Fx.Transitions.extend=function(A){for(var B in A){Fx.Transitions[B]=new Fx.Transition(A[B]);
Fx.Transitions.compat(B);}};Fx.Transitions.compat=function(A){["In","Out","InOut"].each(function(B){Fx.Transitions[A.toLowerCase()+B]=Fx.Transitions[A]["ease"+B];
});};Fx.Transitions.extend({Pow:function(B,A){return Math.pow(B,A[0]||6);},Expo:function(A){return Math.pow(2,8*(A-1));},Circ:function(A){return 1-Math.sin(Math.acos(A));
},Sine:function(A){return 1-Math.sin((1-A)*Math.PI/2);},Back:function(B,A){A=A[0]||1.618;return Math.pow(B,2)*((A+1)*B-A);},Bounce:function(D){var C;for(var B=0,A=1;
1;B+=A,A/=2){if(D>=(7-4*B)/11){C=-Math.pow((11-6*B-11*D)/4,2)+A*A;break;}}return C;},Elastic:function(B,A){return Math.pow(2,10*--B)*Math.cos(20*B*Math.PI*(A[0]||1)/3);
}});["Quad","Cubic","Quart","Quint"].each(function(B,A){Fx.Transitions[B]=new Fx.Transition(function(C){return Math.pow(C,[A+2]);});Fx.Transitions.compat(B);
});var Drag={};Drag.Base=new Class({options:{handle:false,unit:"px",onStart:Class.empty,onBeforeStart:Class.empty,onComplete:Class.empty,onSnap:Class.empty,onDrag:Class.empty,limit:false,modifiers:{x:"left",y:"top"},grid:false,snap:6},initialize:function(B,A){this.setOptions(A);
this.element=$(B);this.handle=$(this.options.handle)||this.element;this.mouse={now:{},pos:{}};this.value={start:{},now:{}};this.bound={start:this.start.bindWithEvent(this),check:this.check.bindWithEvent(this),drag:this.drag.bindWithEvent(this),stop:this.stop.bind(this)};
this.attach();if(this.options.initialize){this.options.initialize.call(this);}},attach:function(){this.handle.addEvent("mousedown",this.bound.start);return this;
},detach:function(){this.handle.removeEvent("mousedown",this.bound.start);return this;},start:function(C){this.fireEvent("onBeforeStart",this.element);
this.mouse.start=C.page;var A=this.options.limit;this.limit={x:[],y:[]};for(var D in this.options.modifiers){if(!this.options.modifiers[D]){continue;}this.value.now[D]=this.element.getStyle(this.options.modifiers[D]).toInt();
this.mouse.pos[D]=C.page[D]-this.value.now[D];if(A&&A[D]){for(var B=0;B<2;B++){if($chk(A[D][B])){this.limit[D][B]=($type(A[D][B])=="function")?A[D][B]():A[D][B];
}}}}if($type(this.options.grid)=="number"){this.options.grid={x:this.options.grid,y:this.options.grid};}document.addListener("mousemove",this.bound.check);
document.addListener("mouseup",this.bound.stop);this.fireEvent("onStart",this.element);C.stop();},check:function(A){var B=Math.round(Math.sqrt(Math.pow(A.page.x-this.mouse.start.x,2)+Math.pow(A.page.y-this.mouse.start.y,2)));
if(B>this.options.snap){document.removeListener("mousemove",this.bound.check);document.addListener("mousemove",this.bound.drag);this.drag(A);this.fireEvent("onSnap",this.element);
}A.stop();},drag:function(A){this.out=false;this.mouse.now=A.page;for(var B in this.options.modifiers){if(!this.options.modifiers[B]){continue;}this.value.now[B]=this.mouse.now[B]-this.mouse.pos[B];
if(this.limit[B]){if($chk(this.limit[B][1])&&(this.value.now[B]>this.limit[B][1])){this.value.now[B]=this.limit[B][1];this.out=true;}else{if($chk(this.limit[B][0])&&(this.value.now[B]<this.limit[B][0])){this.value.now[B]=this.limit[B][0];
this.out=true;}}}if(this.options.grid[B]){this.value.now[B]-=(this.value.now[B]%this.options.grid[B]);}this.element.setStyle(this.options.modifiers[B],this.value.now[B]+this.options.unit);
}this.fireEvent("onDrag",this.element);A.stop();},stop:function(){document.removeListener("mousemove",this.bound.check);document.removeListener("mousemove",this.bound.drag);
document.removeListener("mouseup",this.bound.stop);this.fireEvent("onComplete",this.element);}});Drag.Base.implement(new Events,new Options);Element.extend({makeResizable:function(A){return new Drag.Base(this,$merge({modifiers:{x:"width",y:"height"}},A));
}});Drag.Move=Drag.Base.extend({options:{droppables:[],container:false,overflown:[]},initialize:function(B,A){this.setOptions(A);this.element=$(B);this.droppables=$$(this.options.droppables);
this.container=$(this.options.container);this.position={element:this.element.getStyle("position"),container:false};if(this.container){this.position.container=this.container.getStyle("position");
}if(!["relative","absolute","fixed"].contains(this.position.element)){this.position.element="absolute";}var D=this.element.getStyle("top").toInt();var C=this.element.getStyle("left").toInt();
if(this.position.element=="absolute"&&!["relative","absolute","fixed"].contains(this.position.container)){D=$chk(D)?D:this.element.getTop(this.options.overflown);
C=$chk(C)?C:this.element.getLeft(this.options.overflown);}else{D=$chk(D)?D:0;C=$chk(C)?C:0;}this.element.setStyles({top:D,left:C,position:this.position.element});
this.parent(this.element);},start:function(C){this.overed=null;if(this.container){var A=this.container.getCoordinates();var B=this.element.getCoordinates();
if(this.position.element=="absolute"&&!["relative","absolute","fixed"].contains(this.position.container)){this.options.limit={x:[A.left,A.right-B.width],y:[A.top,A.bottom-B.height]};
}else{this.options.limit={y:[0,A.height-B.height],x:[0,A.width-B.width]};}}this.parent(C);},drag:function(A){this.parent(A);var B=this.out?false:this.droppables.filter(this.checkAgainst,this).getLast();
if(this.overed!=B){if(this.overed){this.overed.fireEvent("leave",[this.element,this]);}this.overed=B?B.fireEvent("over",[this.element,this]):null;}return this;
},checkAgainst:function(B){B=B.getCoordinates(this.options.overflown);var A=this.mouse.now;return(A.x>B.left&&A.x<B.right&&A.y<B.bottom&&A.y>B.top);},stop:function(){if(this.overed&&!this.out){this.overed.fireEvent("drop",[this.element,this]);
}else{this.element.fireEvent("emptydrop",this);}this.parent();return this;}});Element.extend({makeDraggable:function(A){return new Drag.Move(this,A);}});
var XHR=new Class({options:{method:"post",async:true,onRequest:Class.empty,onSuccess:Class.empty,onFailure:Class.empty,urlEncoded:true,encoding:"utf-8",autoCancel:false,headers:{}},setTransport:function(){this.transport=(window.XMLHttpRequest)?new XMLHttpRequest():(window.ie?new ActiveXObject("Microsoft.XMLHTTP"):false);
return this;},initialize:function(A){this.setTransport().setOptions(A);this.options.isSuccess=this.options.isSuccess||this.isSuccess;this.headers={};if(this.options.urlEncoded&&this.options.method=="post"){var B=(this.options.encoding)?"; charset="+this.options.encoding:"";
this.setHeader("Content-type","application/x-www-form-urlencoded"+B);}if(this.options.initialize){this.options.initialize.call(this);}},onStateChange:function(){if(this.transport.readyState!=4||!this.running){return ;
}this.running=false;var A=0;try{A=this.transport.status;}catch(B){}if(this.options.isSuccess.call(this,A)){this.onSuccess();}else{this.onFailure();}this.transport.onreadystatechange=Class.empty;
},isSuccess:function(A){return((A>=200)&&(A<300));},onSuccess:function(){this.response={text:this.transport.responseText,xml:this.transport.responseXML};
this.fireEvent("onSuccess",[this.response.text,this.response.xml]);this.callChain();},onFailure:function(){this.fireEvent("onFailure",this.transport);},setHeader:function(A,B){this.headers[A]=B;
return this;},send:function(A,C){if(this.options.autoCancel){this.cancel();}else{if(this.running){return this;}}this.running=true;if(C&&this.options.method=="get"){A=A+(A.contains("?")?"&":"?")+C;
C=null;}this.transport.open(this.options.method.toUpperCase(),A,this.options.async);this.transport.onreadystatechange=this.onStateChange.bind(this);if((this.options.method=="post")&&this.transport.overrideMimeType){this.setHeader("Connection","close");
}$extend(this.headers,this.options.headers);for(var B in this.headers){try{this.transport.setRequestHeader(B,this.headers[B]);}catch(D){}}this.fireEvent("onRequest");
this.transport.send($pick(C,null));return this;},cancel:function(){if(!this.running){return this;}this.running=false;this.transport.abort();this.transport.onreadystatechange=Class.empty;
this.setTransport();this.fireEvent("onCancel");return this;}});XHR.implement(new Chain,new Events,new Options);var Ajax=XHR.extend({options:{data:null,update:null,onComplete:Class.empty,evalScripts:false,evalResponse:false},initialize:function(B,A){this.addEvent("onSuccess",this.onComplete);
this.setOptions(A);this.options.data=this.options.data||this.options.postBody;if(!["post","get"].contains(this.options.method)){this._method="_method="+this.options.method;
this.options.method="post";}this.parent();this.setHeader("X-Requested-With","XMLHttpRequest");this.setHeader("Accept","text/javascript, text/html, application/xml, text/xml, */*");
this.url=B;},onComplete:function(){if(this.options.update){$(this.options.update).empty().setHTML(this.response.text);}if(this.options.evalScripts||this.options.evalResponse){this.evalScripts();
}this.fireEvent("onComplete",[this.response.text,this.response.xml],20);},request:function(A){A=A||this.options.data;switch($type(A)){case"element":A=$(A).toQueryString();
break;case"object":A=Object.toQueryString(A);}if(this._method){A=(A)?[this._method,A].join("&"):this._method;}return this.send(this.url,A);},evalScripts:function(){var B,A;
if(this.options.evalResponse||(/(ecma|java)script/).test(this.getHeader("Content-type"))){A=this.response.text;}else{A=[];var C=/<script[^>]*>([\s\S]*?)<\/script>/gi;
while((B=C.exec(this.response.text))){A.push(B[1]);}A=A.join("\n");}if(A){(window.execScript)?window.execScript(A):window.setTimeout(A,0);}},getHeader:function(A){try{return this.transport.getResponseHeader(A);
}catch(B){}return null;}});Object.toQueryString=function(B){var C=[];for(var A in B){C.push(encodeURIComponent(A)+"="+encodeURIComponent(B[A]));}return C.join("&");
};Element.extend({send:function(A){return new Ajax(this.getProperty("action"),$merge({data:this.toQueryString()},A,{method:"post"})).request();}});var Cookie=new Abstract({options:{domain:false,path:false,duration:false,secure:false},set:function(C,D,B){B=$merge(this.options,B);
D=encodeURIComponent(D);if(B.domain){D+="; domain="+B.domain;}if(B.path){D+="; path="+B.path;}if(B.duration){var A=new Date();A.setTime(A.getTime()+B.duration*24*60*60*1000);
D+="; expires="+A.toGMTString();}if(B.secure){D+="; secure";}document.cookie=C+"="+D;return $extend(B,{key:C,value:D});},get:function(A){var B=document.cookie.match("(?:^|;)\\s*"+A.escapeRegExp()+"=([^;]*)");
return B?decodeURIComponent(B[1]):false;},remove:function(B,A){if($type(B)=="object"){this.set(B.key,"",$merge(B,{duration:-1}));}else{this.set(B,"",$merge(A,{duration:-1}));
}}});var Json={toString:function(C){switch($type(C)){case"string":return'"'+C.replace(/(["\\])/g,"\\$1")+'"';case"array":return"["+C.map(Json.toString).join(",")+"]";
case"object":var A=[];for(var B in C){A.push(Json.toString(B)+":"+Json.toString(C[B]));}return"{"+A.join(",")+"}";case"number":if(isFinite(C)){break;}case false:return"null";
}return String(C);},evaluate:function(str,secure){return(($type(str)!="string")||(secure&&!str.test(/^("(\\.|[^"\\\n\r])*?"|[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t])+?$/)))?null:eval("("+str+")");
}};Json.Remote=XHR.extend({initialize:function(B,A){this.url=B;this.addEvent("onSuccess",this.onComplete);this.parent(A);this.setHeader("X-Request","JSON");
},send:function(A){return this.parent(this.url,"json="+Json.toString(A));},onComplete:function(){this.fireEvent("onComplete",[Json.evaluate(this.response.text,this.options.secure)]);
}});var Asset=new Abstract({javascript:function(C,B){B=$merge({onload:Class.empty},B);var A=new Element("script",{src:C}).addEvents({load:B.onload,readystatechange:function(){if(this.readyState=="complete"){this.fireEvent("load");
}}});delete B.onload;return A.setProperties(B).inject(document.head);},css:function(B,A){return new Element("link",$merge({rel:"stylesheet",media:"screen",type:"text/css",href:B},A)).inject(document.head);
},image:function(C,B){B=$merge({onload:Class.empty,onabort:Class.empty,onerror:Class.empty},B);var D=new Image();D.src=C;var A=new Element("img",{src:C});
["load","abort","error"].each(function(E){var F=B["on"+E];delete B["on"+E];A.addEvent(E,function(){this.removeEvent(E,arguments.callee);F.call(this);});
});if(D.width&&D.height){A.fireEvent("load",A,1);}return A.setProperties(B);},images:function(D,C){C=$merge({onComplete:Class.empty,onProgress:Class.empty},C);
if(!D.push){D=[D];}var A=[];var B=0;D.each(function(F){var E=new Asset.image(F,{onload:function(){C.onProgress.call(this,B);B++;if(B==D.length){C.onComplete();
}}});A.push(E);});return new Elements(A);}});var Hash=new Class({length:0,initialize:function(A){this.obj=A||{};this.setLength();},get:function(A){return(this.hasKey(A))?this.obj[A]:null;
},hasKey:function(A){return(A in this.obj);},set:function(A,B){if(!this.hasKey(A)){this.length++;}this.obj[A]=B;return this;},setLength:function(){this.length=0;
for(var A in this.obj){this.length++;}return this;},remove:function(A){if(this.hasKey(A)){delete this.obj[A];this.length--;}return this;},each:function(A,B){$each(this.obj,A,B);
},extend:function(A){$extend(this.obj,A);return this.setLength();},merge:function(){this.obj=$merge.apply(null,[this.obj].extend(arguments));return this.setLength();
},empty:function(){this.obj={};this.length=0;return this;},keys:function(){var A=[];for(var B in this.obj){A.push(B);}return A;},values:function(){var A=[];
for(var B in this.obj){A.push(this.obj[B]);}return A;}});function $H(A){return new Hash(A);}Hash.Cookie=Hash.extend({initialize:function(B,A){this.name=B;
this.options=$extend({autoSave:true},A||{});this.load();},save:function(){if(this.length==0){Cookie.remove(this.name,this.options);return true;}var A=Json.toString(this.obj);
if(A.length>4096){return false;}Cookie.set(this.name,A,this.options);return true;},load:function(){this.obj=Json.evaluate(Cookie.get(this.name),true)||{};
this.setLength();}});Hash.Cookie.Methods={};["extend","set","merge","empty","remove"].each(function(A){Hash.Cookie.Methods[A]=function(){Hash.prototype[A].apply(this,arguments);
if(this.options.autoSave){this.save();}return this;};});Hash.Cookie.implement(Hash.Cookie.Methods);var Color=new Class({initialize:function(B,D){D=D||(B.push?"rgb":"hex");
var C,A;switch(D){case"rgb":C=B;A=C.rgbToHsb();break;case"hsb":C=B.hsbToRgb();A=B;break;default:C=B.hexToRgb(true);A=C.rgbToHsb();}C.hsb=A;C.hex=C.rgbToHex();
return $extend(C,Color.prototype);},mix:function(){var A=$A(arguments);var C=($type(A[A.length-1])=="number")?A.pop():50;var B=this.copy();A.each(function(D){D=new Color(D);
for(var E=0;E<3;E++){B[E]=Math.round((B[E]/100*(100-C))+(D[E]/100*C));}});return new Color(B,"rgb");},invert:function(){return new Color(this.map(function(A){return 255-A;
}));},setHue:function(A){return new Color([A,this.hsb[1],this.hsb[2]],"hsb");},setSaturation:function(A){return new Color([this.hsb[0],A,this.hsb[2]],"hsb");
},setBrightness:function(A){return new Color([this.hsb[0],this.hsb[1],A],"hsb");}});function $RGB(C,B,A){return new Color([C,B,A],"rgb");}function $HSB(C,B,A){return new Color([C,B,A],"hsb");
}Array.extend({rgbToHsb:function(){var B=this[0],C=this[1],J=this[2];var G,F,H;var I=Math.max(B,C,J),E=Math.min(B,C,J);var K=I-E;H=I/255;F=(I!=0)?K/I:0;
if(F==0){G=0;}else{var D=(I-B)/K;var A=(I-C)/K;var L=(I-J)/K;if(B==I){G=L-A;}else{if(C==I){G=2+D-L;}else{G=4+A-D;}}G/=6;if(G<0){G++;}}return[Math.round(G*360),Math.round(F*100),Math.round(H*100)];
},hsbToRgb:function(){var C=Math.round(this[2]/100*255);if(this[1]==0){return[C,C,C];}else{var A=this[0]%360;var E=A%60;var F=Math.round((this[2]*(100-this[1]))/10000*255);
var D=Math.round((this[2]*(6000-this[1]*E))/600000*255);var B=Math.round((this[2]*(6000-this[1]*(60-E)))/600000*255);switch(Math.floor(A/60)){case 0:return[C,B,F];
case 1:return[D,C,F];case 2:return[F,C,B];case 3:return[F,D,C];case 4:return[B,F,C];case 5:return[C,F,D];}}return false;}});var Scroller=new Class({options:{area:20,velocity:1,onChange:function(A,B){this.element.scrollTo(A,B);
}},initialize:function(B,A){this.setOptions(A);this.element=$(B);this.mousemover=([window,document].contains(B))?$(document.body):this.element;},start:function(){this.coord=this.getCoords.bindWithEvent(this);
this.mousemover.addListener("mousemove",this.coord);},stop:function(){this.mousemover.removeListener("mousemove",this.coord);this.timer=$clear(this.timer);
},getCoords:function(A){this.page=(this.element==window)?A.client:A.page;if(!this.timer){this.timer=this.scroll.periodical(50,this);}},scroll:function(){var A=this.element.getSize();
var D=this.element.getPosition();var C={x:0,y:0};for(var B in this.page){if(this.page[B]<(this.options.area+D[B])&&A.scroll[B]!=0){C[B]=(this.page[B]-this.options.area-D[B])*this.options.velocity;
}else{if(this.page[B]+this.options.area>(A.size[B]+D[B])&&A.scroll[B]+A.size[B]!=A.scrollSize[B]){C[B]=(this.page[B]-A.size[B]+this.options.area-D[B])*this.options.velocity;
}}}if(C.y||C.x){this.fireEvent("onChange",[A.scroll.x+C.x,A.scroll.y+C.y]);}}});Scroller.implement(new Events,new Options);var Slider=new Class({options:{onChange:Class.empty,onComplete:Class.empty,onTick:function(A){this.knob.setStyle(this.p,A);
},mode:"horizontal",steps:100,offset:0},initialize:function(D,A,B){this.element=$(D);this.knob=$(A);this.setOptions(B);this.previousChange=-1;this.previousEnd=-1;
this.step=-1;this.element.addEvent("mousedown",this.clickedElement.bindWithEvent(this));var C,F;switch(this.options.mode){case"horizontal":this.z="x";this.p="left";
C={x:"left",y:false};F="offsetWidth";break;case"vertical":this.z="y";this.p="top";C={x:false,y:"top"};F="offsetHeight";}this.max=this.element[F]-this.knob[F]+(this.options.offset*2);
this.half=this.knob[F]/2;this.getPos=this.element["get"+this.p.capitalize()].bind(this.element);this.knob.setStyle("position","relative").setStyle(this.p,-this.options.offset);
var E={};E[this.z]=[-this.options.offset,this.max-this.options.offset];this.drag=new Drag.Base(this.knob,{limit:E,modifiers:C,snap:0,onStart:function(){this.draggedKnob();
}.bind(this),onDrag:function(){this.draggedKnob();}.bind(this),onComplete:function(){this.draggedKnob();this.end();}.bind(this)});if(this.options.initialize){this.options.initialize.call(this);
}},set:function(A){this.step=A.limit(0,this.options.steps);this.checkStep();this.end();this.fireEvent("onTick",this.toPosition(this.step));return this;
},clickedElement:function(B){var A=B.page[this.z]-this.getPos()-this.half;A=A.limit(-this.options.offset,this.max-this.options.offset);this.step=this.toStep(A);
this.checkStep();this.end();this.fireEvent("onTick",A);},draggedKnob:function(){this.step=this.toStep(this.drag.value.now[this.z]);this.checkStep();},checkStep:function(){if(this.previousChange!=this.step){this.previousChange=this.step;
this.fireEvent("onChange",this.step);}},end:function(){if(this.previousEnd!==this.step){this.previousEnd=this.step;this.fireEvent("onComplete",this.step+"");
}},toStep:function(A){return Math.round((A+this.options.offset)/this.max*this.options.steps);},toPosition:function(A){return this.max*A/this.options.steps;
}});Slider.implement(new Events);Slider.implement(new Options);var SmoothScroll=Fx.Scroll.extend({initialize:function(B){this.parent(window,B);this.links=(this.options.links)?$$(this.options.links):$$(document.links);
var A=window.location.href.match(/^[^#]*/)[0]+"#";this.links.each(function(D){if(D.href.indexOf(A)!=0){return ;}var C=D.href.substr(A.length);if(C&&$(C)){this.useLink(D,C);
}},this);if(!window.webkit419){this.addEvent("onComplete",function(){window.location.hash=this.anchor;});}},useLink:function(B,A){B.addEvent("click",function(C){this.anchor=A;
this.toElement(A);C.stop();}.bindWithEvent(this));}});var Sortables=new Class({options:{handles:false,onStart:Class.empty,onComplete:Class.empty,ghost:true,snap:3,onDragStart:function(A,B){B.setStyle("opacity",0.7);
A.setStyle("opacity",0.7);},onDragComplete:function(A,B){A.setStyle("opacity",1);B.remove();this.trash.remove();}},initialize:function(D,B){this.setOptions(B);
this.list=$(D);this.elements=this.list.getChildren();this.handles=(this.options.handles)?$$(this.options.handles):this.elements;this.bound={start:[],moveGhost:this.moveGhost.bindWithEvent(this)};
for(var C=0,A=this.handles.length;C<A;C++){this.bound.start[C]=this.start.bindWithEvent(this,this.elements[C]);}this.attach();if(this.options.initialize){this.options.initialize.call(this);
}this.bound.move=this.move.bindWithEvent(this);this.bound.end=this.end.bind(this);},attach:function(){this.handles.each(function(B,A){B.addEvent("mousedown",this.bound.start[A]);
},this);},detach:function(){this.handles.each(function(B,A){B.removeEvent("mousedown",this.bound.start[A]);},this);},start:function(C,B){this.active=B;
this.coordinates=this.list.getCoordinates();if(this.options.ghost){var A=B.getPosition();this.offset=C.page.y-A.y;this.trash=new Element("div").inject(document.body);
this.ghost=B.clone().inject(this.trash).setStyles({position:"absolute",left:A.x,top:C.page.y-this.offset});document.addListener("mousemove",this.bound.moveGhost);
this.fireEvent("onDragStart",[B,this.ghost]);}document.addListener("mousemove",this.bound.move);document.addListener("mouseup",this.bound.end);this.fireEvent("onStart",B);
C.stop();},moveGhost:function(A){var B=A.page.y-this.offset;B=B.limit(this.coordinates.top,this.coordinates.bottom-this.ghost.offsetHeight);this.ghost.setStyle("top",B);
A.stop();},move:function(E){var B=E.page.y;this.previous=this.previous||B;var A=((this.previous-B)>0);var D=this.active.getPrevious();var C=this.active.getNext();
if(D&&A&&B<D.getCoordinates().bottom){this.active.injectBefore(D);}if(C&&!A&&B>C.getCoordinates().top){this.active.injectAfter(C);}this.previous=B;},serialize:function(A){return this.list.getChildren().map(A||function(B){return this.elements.indexOf(B);
},this);},end:function(){this.previous=null;document.removeListener("mousemove",this.bound.move);document.removeListener("mouseup",this.bound.end);if(this.options.ghost){document.removeListener("mousemove",this.bound.moveGhost);
this.fireEvent("onDragComplete",[this.active,this.ghost]);}this.fireEvent("onComplete",this.active);}});Sortables.implement(new Events,new Options);var Tips=new Class({options:{onShow:function(A){A.setStyle("visibility","visible");
},onHide:function(A){A.setStyle("visibility","hidden");},maxTitleChars:30,showDelay:100,hideDelay:100,className:"tool",offsets:{x:16,y:16},fixed:false},initialize:function(B,A){this.setOptions(A);
this.toolTip=new Element("div",{"class":this.options.className+"-tip",styles:{position:"absolute",top:"0",left:"0",visibility:"hidden"}}).inject(document.body);
this.wrapper=new Element("div").inject(this.toolTip);$$(B).each(this.build,this);if(this.options.initialize){this.options.initialize.call(this);}},build:function(B){B.$tmp.myTitle=(B.href&&B.getTag()=="a")?B.href.replace("http://",""):(B.rel||false);
if(B.title){var C=B.title.split("::");if(C.length>1){B.$tmp.myTitle=C[0].trim();B.$tmp.myText=C[1].trim();}else{B.$tmp.myText=B.title;}B.removeAttribute("title");
}else{B.$tmp.myText=false;}if(B.$tmp.myTitle&&B.$tmp.myTitle.length>this.options.maxTitleChars){B.$tmp.myTitle=B.$tmp.myTitle.substr(0,this.options.maxTitleChars-1)+"&hellip;";
}B.addEvent("mouseenter",function(D){this.start(B);if(!this.options.fixed){this.locate(D);}else{this.position(B);}}.bind(this));if(!this.options.fixed){B.addEvent("mousemove",this.locate.bindWithEvent(this));
}var A=this.end.bind(this);B.addEvent("mouseleave",A);B.addEvent("trash",A);},start:function(A){this.wrapper.empty();if(A.$tmp.myTitle){this.title=new Element("span").inject(new Element("div",{"class":this.options.className+"-title"}).inject(this.wrapper)).setHTML(A.$tmp.myTitle);
}if(A.$tmp.myText){this.text=new Element("span").inject(new Element("div",{"class":this.options.className+"-text"}).inject(this.wrapper)).setHTML(A.$tmp.myText);
}$clear(this.timer);this.timer=this.show.delay(this.options.showDelay,this);},end:function(A){$clear(this.timer);this.timer=this.hide.delay(this.options.hideDelay,this);
},position:function(A){var B=A.getPosition();this.toolTip.setStyles({left:B.x+this.options.offsets.x,top:B.y+this.options.offsets.y});},locate:function(B){var D={x:window.getWidth(),y:window.getHeight()};
var A={x:window.getScrollLeft(),y:window.getScrollTop()};var C={x:this.toolTip.offsetWidth,y:this.toolTip.offsetHeight};var G={x:"left",y:"top"};for(var E in G){var F=B.page[E]+this.options.offsets[E];
if((F+C[E]-A[E])>D[E]){F=B.page[E]-this.options.offsets[E]-C[E];}this.toolTip.setStyle(G[E],F);}},show:function(){if(this.options.timeout){this.timer=this.hide.delay(this.options.timeout,this);
}this.fireEvent("onShow",[this.toolTip]);},hide:function(){this.fireEvent("onHide",[this.toolTip]);}});Tips.implement(new Events,new Options);var Group=new Class({initialize:function(){this.instances=$A(arguments);
this.events={};this.checker={};},addEvent:function(B,A){this.checker[B]=this.checker[B]||{};this.events[B]=this.events[B]||[];if(this.events[B].contains(A)){return false;
}else{this.events[B].push(A);}this.instances.each(function(C,D){C.addEvent(B,this.check.bind(this,[B,C,D]));},this);return this;},check:function(C,A,B){this.checker[C][B]=true;
var D=this.instances.every(function(F,E){return this.checker[C][E]||false;},this);if(!D){return ;}this.checker[C]={};this.events[C].each(function(E){E.call(this,this.instances,A);
},this);}});var Accordion=Fx.Elements.extend({options:{onActive:Class.empty,onBackground:Class.empty,display:0,show:false,height:true,width:false,opacity:true,fixedHeight:false,fixedWidth:false,wait:false,alwaysHide:false},initialize:function(){var C,E,F,B;
$each(arguments,function(I,H){switch($type(I)){case"object":C=I;break;case"element":B=$(I);break;default:var G=$$(I);if(!E){E=G;}else{F=G;}}});this.togglers=E||[];
this.elements=F||[];this.container=$(B);this.setOptions(C);this.previous=-1;if(this.options.alwaysHide){this.options.wait=true;}if($chk(this.options.show)){this.options.display=false;
this.previous=this.options.show;}if(this.options.start){this.options.display=false;this.options.show=false;}this.effects={};if(this.options.opacity){this.effects.opacity="fullOpacity";
}if(this.options.width){this.effects.width=this.options.fixedWidth?"fullWidth":"offsetWidth";}if(this.options.height){this.effects.height=this.options.fixedHeight?"fullHeight":"scrollHeight";
}for(var D=0,A=this.togglers.length;D<A;D++){this.addSection(this.togglers[D],this.elements[D]);}this.elements.each(function(H,G){if(this.options.show===G){this.fireEvent("onActive",[this.togglers[G],H]);
}else{for(var I in this.effects){H.setStyle(I,0);}}},this);this.parent(this.elements);if($chk(this.options.display)){this.display(this.options.display);
}},addSection:function(E,C,G){E=$(E);C=$(C);var F=this.togglers.contains(E);var B=this.togglers.length;this.togglers.include(E);this.elements.include(C);
if(B&&(!F||G)){G=$pick(G,B-1);E.injectBefore(this.togglers[G]);C.injectAfter(E);}else{if(this.container&&!F){E.inject(this.container);C.inject(this.container);
}}var A=this.togglers.indexOf(E);E.addEvent("click",this.display.bind(this,A));if(this.options.height){C.setStyles({"padding-top":0,"border-top":"none","padding-bottom":0,"border-bottom":"none"});
}if(this.options.width){C.setStyles({"padding-left":0,"border-left":"none","padding-right":0,"border-right":"none"});}C.fullOpacity=1;if(this.options.fixedWidth){C.fullWidth=this.options.fixedWidth;
}if(this.options.fixedHeight){C.fullHeight=this.options.fixedHeight;}C.setStyle("overflow","hidden");if(!F){for(var D in this.effects){C.setStyle(D,0);
}}return this;},display:function(A){A=($type(A)=="element")?this.elements.indexOf(A):A;if((this.timer&&this.options.wait)||(A===this.previous&&!this.options.alwaysHide)){return this;
}this.previous=A;var B={};this.elements.each(function(E,D){B[D]={};var C=(D!=A)||(this.options.alwaysHide&&(E.offsetHeight>0));this.fireEvent(C?"onBackground":"onActive",[this.togglers[D],E]);
for(var F in this.effects){B[D][F]=C?0:E[this.effects[F]];}},this);return this.start(B);},showThisHideOpen:function(A){return this.display(A);}});Fx.Accordion=Accordion;
var mootabs = new Class({
 initialize: function(element, options) {
 this.options = Object.extend({
 width: '300px',
 height: '200px',
 changeTransition: Fx.Transitions.Bounce.easeOut,
 duration: 1000,
 mouseOverClass: 'active',
 activateOnLoad: 'first',
 useAjax: false,
 ajaxUrl: '',
 ajaxOptions: {method:'get'},
 ajaxLoadingText: 'Loading...'
 }, options || {});

 this.el = $(element);
 this.elid = element;

 this.el.setStyles({
 height: this.options.height,
 width: this.options.width
 });

 this.titles = $$('#' + this.elid + ' ul li.toogler');
 this.panelHeight = this.el.getSize().size.y - (this.titles[0].getSize().size.y + 4);
 this.panels = $$('#' + this.elid + ' .mootabs_panel');

 this.panels.setStyle('height', this.panelHeight);

 this.titles.each(function(item) {
 item.addEvent('click', function(){
 item.removeClass(this.options.mouseOverClass);
 this.activate(item);
 }.bind(this)
 );

 item.addEvent('mouseover', function() {
 if(item != this.activeTitle)
 {
 item.addClass(this.options.mouseOverClass);
 }
 }.bind(this));

 item.addEvent('mouseout', function() {
 if(item != this.activeTitle)
 {
 item.removeClass(this.options.mouseOverClass);
 }
 }.bind(this));
 }.bind(this));

 if(this.options.activateOnLoad != 'none')
 {
 if(this.options.activateOnLoad == 'first')
 {
 this.activate(this.titles[0], true);
 }
 else
 {
 this.activate(this.options.activateOnLoad, true); 
 }
 }
 },

 activate: function(tab, skipAnim){
 if(! $defined(skipAnim))
 {
 skipAnim = false;
 }
 if($type(tab) == 'string') 
 {
 myTab = $$('#' + this.elid + ' ul li.toogler').filterByAttribute('title', '=', tab)[0];
 tab = myTab;
 }

 if($type(tab) == 'element')
 {
 var newTab = tab.getProperty('title');
 this.panels.removeClass('active');

 this.activePanel = this.panels.filterById(newTab)[0];

 this.activePanel.addClass('active');

 if(this.options.changeTransition != 'none' && skipAnim==false)
 {
 this.panels.filterById(newTab).setStyle('height', 0);
 var changeEffect = new Fx.Elements(this.panels.filterById(newTab), {duration: this.options.duration, transition: this.options.changeTransition});
 changeEffect.start({
 '0': {
 'height': [0, this.panelHeight]
 }
 });
 }

 this.titles.removeClass('active');

 tab.addClass('active');

 this.activeTitle = tab;

 if(this.options.useAjax)
 {
 this._getContent();
 }
 }
 },

 _getContent: function(){
 this.activePanel.setHTML(this.options.ajaxLoadingText);
 var newOptions = {update: this.activePanel.getProperty('id')};
 this.options.ajaxOptions = Object.extend(this.options.ajaxOptions, newOptions || {});
 var tabRequest = new Ajax(this.options.ajaxUrl + '?tab=' + this.activeTitle.getProperty('title'), this.options.ajaxOptions);
 tabRequest.request();
 },

 addTab: function(title, label, content){
 //the new title
 var newTitle = new Element('li.toogler', {
 'title': title
 });
 newTitle.appendText(label);
 this.titles.include(newTitle);
 $$('#' + this.elid + ' ul').adopt(newTitle);
 newTitle.addEvent('click', function() {
 this.activate(newTitle);
 }.bind(this));

 newTitle.addEvent('mouseover', function() {
 if(newTitle != this.activeTitle)
 {
 newTitle.addClass(this.options.mouseOverClass);
 }
 }.bind(this));
 newTitle.addEvent('mouseout', function() {
 if(newTitle != this.activeTitle)
 {
 newTitle.removeClass(this.options.mouseOverClass);
 }
 }.bind(this));
 //the new panel
 var newPanel = new Element('div', {
 'style': {'height': this.options.panelHeight},
 'id': title,
 'class': 'mootabs_panel'
 });
 if(!this.options.useAjax)
 {
 newPanel.setHTML(content);
 }
 this.panels.include(newPanel);
 this.el.adopt(newPanel);
 },

 removeTab: function(title){
 if(this.activeTitle.title == title)
 {
 this.activate(this.titles[0]);
 }
 $$('#' + this.elid + ' ul li.toogler').filterByAttribute('title', '=', title)[0].remove();

 $$('#' + this.elid + ' .mootabs_panel').filterById(title)[0].remove();
 },

 next: function(){
 var nextTab = this.activeTitle.getNext();
 if(!nextTab) {
 nextTab = this.titles[0];
 }
 this.activate(nextTab);
 },

 previous: function(){
 var previousTab = this.activeTitle.getPrevious();
 if(!previousTab) {
 previousTab = this.titles[this.titles.length - 1];
 }
 this.activate(previousTab);
 }
});/*
 Slimbox v1.41 - The ultimate lightweight Lightbox clone
 by Christophe Beyls (http://www.digitalia.be) - MIT-style license.
 Inspired by the original Lightbox v2 by Lokesh Dhakar.
*/

var Lightbox = {

 init: function(options){
 this.options = $extend({
 resizeDuration: 400,
 resizeTransition: false, // default transition
 initialWidth: 250,
 initialHeight: 250,
 animateCaption: true,
 showCounter: true
 }, options || {});

 this.anchors = [];
 $each(document.links, function(el){
 if (el.rel && el.rel.test(/^lightbox/i)){
 el.onclick = this.click.pass(el, this);
 this.anchors.push(el);
 }
 }, this);
 this.eventKeyDown = this.keyboardListener.bindAsEventListener(this);
 this.eventPosition = this.position.bind(this);

 this.overlay = new Element('div', {'id': 'lbOverlay'}).injectInside(document.body);

 this.center = new Element('div', {'id': 'lbCenter', 'styles': {'width': this.options.initialWidth, 'height': this.options.initialHeight, 'marginLeft': -(this.options.initialWidth/2), 'display': 'none'}}).injectInside(document.body);
 this.image = new Element('div', {'id': 'lbImage'}).injectInside(this.center);
 this.prevLink = new Element('a', {'id': 'lbPrevLink', 'href': '#', 'styles': {'display': 'none'}}).injectInside(this.image);
 this.nextLink = this.prevLink.clone().setProperty('id', 'lbNextLink').injectInside(this.image);
 this.prevLink.onclick = this.previous.bind(this);
 this.nextLink.onclick = this.next.bind(this);

 this.bottomContainer = new Element('div', {'id': 'lbBottomContainer', 'styles': {'display': 'none'}}).injectInside(document.body);
 this.bottom = new Element('div', {'id': 'lbBottom'}).injectInside(this.bottomContainer);
 new Element('a', {'id': 'lbCloseLink', 'href': '#'}).injectInside(this.bottom).onclick = this.overlay.onclick = this.close.bind(this);
 this.caption = new Element('div', {'id': 'lbCaption'}).injectInside(this.bottom);
 this.number = new Element('div', {'id': 'lbNumber'}).injectInside(this.bottom);
 new Element('div', {'styles': {'clear': 'both'}}).injectInside(this.bottom);

 var nextEffect = this.nextEffect.bind(this);
 this.fx = {
 overlay: this.overlay.effect('opacity', {duration: 500}).hide(),
 resize: this.center.effects($extend({duration: this.options.resizeDuration, onComplete: nextEffect}, this.options.resizeTransition ? {transition: this.options.resizeTransition} : {})),
 image: this.image.effect('opacity', {duration: 500, onComplete: nextEffect}),
 bottom: this.bottom.effect('margin-top', {duration: 400, onComplete: nextEffect})
 };

 this.preloadPrev = new Image();
 this.preloadNext = new Image();
 },

 click: function(link){
 if (link.rel.length == 8) return this.show(link.href, link.title);

 var j, imageNum, images = [];
 this.anchors.each(function(el){
 if (el.rel == link.rel){
 for (j = 0; j < images.length; j++) if(images[j][0] == el.href) break;
 if (j == images.length){
 images.push([el.href, el.title]);
 if (el.href == link.href) imageNum = j;
 }
 }
 }, this);
 return this.open(images, imageNum);
 },

 show: function(url, title){
 return this.open([[url, title]], 0);
 },

 open: function(images, imageNum){
 this.images = images;
 this.position();
 this.setup(true);
 this.top = window.getScrollTop() + (window.getHeight() / 15);
 this.center.setStyles({top: this.top, display: ''});
 this.fx.overlay.start(0.8);
 return this.changeImage(imageNum);
 },

 position: function(){
 this.overlay.setStyles({'top': window.getScrollTop(), 'height': window.getHeight()});
 },

 setup: function(open){
 var elements = $A(document.getElementsByTagName('object'));
 elements.extend(document.getElementsByTagName(window.ie ? 'select' : 'embed'));
 elements.each(function(el){
 if (open) el.lbBackupStyle = el.style.visibility;
 el.style.visibility = open ? 'hidden' : el.lbBackupStyle;
 });
 var fn = open ? 'addEvent' : 'removeEvent';
 window[fn]('scroll', this.eventPosition)[fn]('resize', this.eventPosition);
 document[fn]('keydown', this.eventKeyDown);
 this.step = 0;
 },

 keyboardListener: function(event){
 switch (event.keyCode){
 case 27: case 88: case 67: this.close(); break;
 case 37: case 80: this.previous(); break; 
 case 39: case 78: this.next();
 }
 },

 previous: function(){
 return this.changeImage(this.activeImage-1);
 },

 next: function(){
 return this.changeImage(this.activeImage+1);
 },

 changeImage: function(imageNum){
 if (this.step || (imageNum < 0) || (imageNum >= this.images.length)) return false;
 this.step = 1;
 this.activeImage = imageNum;

 this.bottomContainer.style.display = this.prevLink.style.display = this.nextLink.style.display = 'none';
 this.fx.image.hide();
 this.center.className = 'lbLoading';

 this.preload = new Image();
 this.preload.onload = this.nextEffect.bind(this);
 this.preload.src = this.images[imageNum][0];
 return false;
 },

 nextEffect: function(){
 switch (this.step++){
 case 1:
 this.center.className = '';
 this.image.style.backgroundImage = 'url('+this.images[this.activeImage][0]+')';
 this.image.style.width = this.bottom.style.width = this.preload.width+'px';
 this.image.style.height = this.prevLink.style.height = this.nextLink.style.height = this.preload.height+'px';

 this.caption.setHTML(this.images[this.activeImage][1] || '');
 this.number.setHTML((!this.options.showCounter || (this.images.length == 1)) ? '' : 'Image '+(this.activeImage+1)+' of '+this.images.length);

 if (this.activeImage) this.preloadPrev.src = this.images[this.activeImage-1][0];
 if (this.activeImage != (this.images.length - 1)) this.preloadNext.src = this.images[this.activeImage+1][0];
 if (this.center.clientHeight != this.image.offsetHeight){
 this.fx.resize.start({height: this.image.offsetHeight});
 break;
 }
 this.step++;
 case 2:
 if (this.center.clientWidth != this.image.offsetWidth){
 this.fx.resize.start({width: this.image.offsetWidth, marginLeft: -this.image.offsetWidth/2});
 break;
 }
 this.step++;
 case 3:
 this.bottomContainer.setStyles({top: this.top + this.center.clientHeight, height: 0, marginLeft: this.center.style.marginLeft, display: ''});
 this.fx.image.start(1);
 break;
 case 4:
 if (this.options.animateCaption){
 this.fx.bottom.set(-this.bottom.offsetHeight);
 this.bottomContainer.style.height = '';
 this.fx.bottom.start(0);
 break;
 }
 this.bottomContainer.style.height = '';
 case 5:
 if (this.activeImage) this.prevLink.style.display = '';
 if (this.activeImage != (this.images.length - 1)) this.nextLink.style.display = '';
 this.step = 0;
 }
 },

 close: function(){
 if (this.step < 0) return;
 this.step = -1;
 if (this.preload){
 this.preload.onload = Class.empty;
 this.preload = null;
 }
 for (var f in this.fx) this.fx[f].stop();
 this.center.style.display = this.bottomContainer.style.display = 'none';
 this.fx.overlay.chain(this.setup.pass(false, this)).start(0);
 return false;
 }
};

window.addEvent('domready', Lightbox.init.bind(Lightbox));
function hideCart(im){
 window.setTimeout('document.getElementById("mCart").style.display = "none";document.getElementById("'+im+'").src = "img/mini_cart/cart_button.gif";', 2000)
}
function showCart(){
 var cont = document.getElementById("mCart");
 cont.style.display = 'block';
 /*}else{
 hideDelay = window.setTimeout('hideCart()', 1000);
 */
}

function vToggle(tItem){
 if(document.getElementById(tItem).style.display=='none'){
 document.getElementById(tItem).style.display='block';
 }else{
 document.getElementById(tItem).style.display='none';
 }
}

function highlightPromo(item){
 var hItem = document.getElementById("pThumb"+item);
 hItem.className = 'hover';
 for(j=0;j<10;j++){ if(j != item) document.getElementById("pThumb"+j).className = ''; }
 document.getElementById("promoName").innerHTML = promoItems[item]['name'];
 document.getElementById("promoImg").src = promoItems[item]['img'];
 document.getElementById("promoPrice").innerHTML = promoItems[item]['price'];
 document.getElementById("promoSuggest").innerHTML = promoItems[item]['suggested'];
 document.getElementById("promoPrice").innerHTML = promoItems[item]['price'];
 document.getElementById("promoRegular").innerHTML = promoItems[item]['regular'];
 document.getElementById("promoEach5").innerHTML = "for $"+promoItems[item]['each5']+" each";
 document.getElementById("promoEach10").innerHTML = "for $"+promoItems[item]['each10']+" each";
 document.getElementById("promoDesc").innerHTML = promoItems[item]['desc'];
 document.getElementById("promoMorelink").href = promoItems[item]['plink'];
}

function modRate(rating){
 var modSubmit = document.getElementById('mod_submit');
 document.getElementById('curRating').style.width = rating*17+'px';
 document.getElementById('ratingBar').value = rating;
 document.getElementById('rating_score').innerHTML = rating;
}

function checkValue(fld, but, flg){
 var modSubmit = document.getElementById(but);
 var gtg = 0;
 if(fld.value != '') gtg = 1;
 if(flg == 0){
 if($('ratingBar').value > 0){
 if(fld.value != ''){
 gtg = 1;
 }else{
 gtg = 0;
 }
 }
 }

 if(gtg == 1){
 modSubmit.style.color = '#6c6c6c';
 modSubmit.style.cursor = 'pointer';
 }else{
 modSubmit.style.color = '#bdbdbd';
 modSubmit.style.cursor = 'pointer';
 }
}

function checkRatingForm(frm, flg){
 var gtg = 0;
 if($('body').value != '') gtg = 1;
 if(flg == 0){
 if($('ratingBar').value > 0){
 if($('body').value != ''){
 gtg = 1;
 }else{
 gtg = 0;
 alert('You must write a review in order to to rate this extension.');
 }
 }
 }
 if(gtg == 1){
 frm.submit();
 }
}

function selectFeatTab(){
 var tabBut1 = document.getElementById("featScreenshots");
 var tabBut2 = document.getElementById("featScreencasts");
 var tab1 = document.getElementById("featTabScreenshots");
 var tab2 = document.getElementById("featTabScreencasts");
 if(tab1.style.display == 'none'){
 tab2.style.display = 'none';
 tab1.style.display = 'block';
 tabBut2.className = '';
 tabBut1.className = 'active';
 }else{
 tab1.style.display = 'none';
 tab2.style.display = 'block';
 tabBut1.className = '';
 tabBut2.className = 'active';
 }
}

function open_page(page_url, page_title, page_content){
 var paramWin = 'width=500,height=400';
 var oNewWin = window.open('about:blank', '_blank', paramWin);
 
 var sHTML = "<html><head>";
 sHTML += "<title>" + page_title + "</title>";
 sHTML += "</head>";
 sHTML += "<body style='margin:15px;font-family:Arial;font-size:12px;'>";
 sHTML += '<div style="width:470px;">';
 sHTML += "<h3>" + page_title + "</h3>";
 sHTML += "<div style='border:1px solid #eaeaea; background:#f5f5f5; padding:19px;line-height:20px;'>" + page_content + "</div>";
 sHTML += '<div align="center" style="margin:15px;"><input type="button" onclick="window.close();" value="Close" /></div>';
 sHTML += "</div></body></html>";
 oNewWin.document.writeln(sHTML);
}

function str_replace(search, replace, subject) {
 return subject.split(search).join(replace);
}

function checkDownloadSelect (frm){
 for(i=0;i<frm.elements.length;i++){
 var el = frm.elements[i];
 if(el.type == "select-one"){
 if(el.value == ""){
 alert("You must select format in order to continue!");
 return false;
 }
 }
 }
 return true;
}

function convertTime(time){
 if ( time < 59 ) {
 return 'less than a minute ago';
 } 
 else if ( time < 119 ) {
 return 'about a minute ago';
 } 
 else if ( time < 3000 ) {
 return ( parseInt( time / 60 )).toString() + ' minutes ago';
 } 
 else if ( time < 5340 ) {
 return 'about an hour ago';
 } 
 else if ( time < 9000 ) {
 return 'a couple of hours ago'; 
 }
 else if ( time < 82800 ) {
 return 'about ' + ( parseInt( time / 3600 )).toString() + ' hours ago';
 } 
 else if ( time < 129600 ) {
 return 'a day ago';
 }
 else if ( time < 172800 ) {
 return 'almost 2 days ago';
 }
 else {
 return ( parseInt(time / 86400)).toString() + ' days ago';
 }
}

function horizScroller(opt) /* Used in Showcase pages */
{
 var d = 0;
 opt.delay = opt.delay || 50;
 opt.dist = opt.dist || 2;
 opt.leftEl.onmouseover = function() { d = -opt.dist; scroll(); }
 opt.leftEl.onmouseout = function() { d = 0 }
 opt.rightEl.onmouseover = function() { d = opt.dist; scroll(); }
 opt.rightEl.onmouseout = function() { d = 0 }
 
 function scroll()
 {
 if (!d) return;
 var x = opt.contentEl.scrollLeft+d, stop = false, rightBoundary = opt.contentEl.scrollWidth-opt.contentEl.offsetWidth;
 if (x<0) {
 addClassName(opt.leftEl, 'disabled');
 x = 0;
 stop = true;
 } else {
 removeClassName(opt.leftEl, 'disabled');
 }
 if (x>=rightBoundary) {
 addClassName(opt.rightEl, 'disabled');
 x = rightBoundary;
 stop = true;
 } else {
 removeClassName(opt.rightEl, 'disabled');
 }
 opt.contentEl.scrollLeft = x;
 if (stop) return;
 window.setTimeout(scroll, opt.delay);
 }
 
 function addClassName(el, className)
 {
 el.className = el.className.replace(new RegExp(' ?'+className+'|$'), ' '+className);
 }
 
 function removeClassName(el, className)
 {
 el.className = el.className.replace(new RegExp(' ?'+className+'( |$)'), '$1');
 }
}


function fetchURL(form_name)
{
 var ele = document.getElementById(form_name);
 goURL = ele.options[ele.selectedIndex].value;
 if (goURL) location.href = goURL;
}

function showPaymentList(objId, lnk)
{
 obj = document.getElementById(objId);
 if( obj.style.display == 'block' ){
 obj.style.display = 'none';
 lnk.innerHTML = '[ + ] view all supported payment gateways';
 }else{
 obj.style.display = 'block';
 lnk.innerHTML = '[ - ] hide all supported payment gateways';
 }
}

function findPos(obj) {
 var curleft = curtop = 0;
 if (obj.offsetParent) {
 do {
 curleft += obj.offsetLeft;
 curtop += obj.offsetTop;
 } while (obj = obj.offsetParent);
 return [curleft,curtop];
 }
}

function extensionsLinksTrack(divId,trackUrl,memberId){
 var container = $(divId);
 var links = container.getElementsByTagName('a');
 if(links.length > 0){
 for( i=0;i<links.length;i++ ){
 if(links[i].href != ''){
 links[i].target = "_blank";
 links[i].onclick = function(){
 if(memberId > 0){
 new Ajax(trackUrl, {method: 'get'}).request();
 }else{
 showLoginMessage(this);
 return false;
 }
 }
 }
 }
 }
}

function loadScript(url, callback){
 var script = document.createElement("script")
 script.type = "text/javascript";
 if (script.readyState){ //IE
 script.onreadystatechange = function(){
 if (script.readyState == "loaded" ||
 script.readyState == "complete"){
 script.onreadystatechange = null;
 //callback();
 }
 };
 } else {
 script.onload = function(){
 //callback();
 };
 }
 script.src = url;
 document.getElementsByTagName("head")[0].appendChild(script);
} 

function loadStyles(url){
 var link = document.createElement("link");
 link.rel = 'stylesheet'
 link.type = "text/css";
 if (link.readyState){ //IE
 link.onreadystatechange = function(){
 if (link.readyState == "loaded" ||
 link.readyState == "complete"){
 link.onreadystatechange = null;
 }
 };
 }

 link.href = url;
 document.getElementsByTagName("head")[0].appendChild(link);
} 

function getWindowSize(){
 var viewportwidth;
 var viewportheight;
 
 // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
 
 if (typeof window.innerWidth != 'undefined')
 {
 viewportwidth = window.innerWidth,
 viewportheight = window.innerHeight
 }
 
 // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)

 else if (typeof document.documentElement != 'undefined'
 && typeof document.documentElement.clientWidth !=
 'undefined' && document.documentElement.clientWidth != 0)
 {
 viewportwidth = document.documentElement.clientWidth,
 viewportheight = document.documentElement.clientHeight
 }
 
 // older versions of IE
 
 else
 {
 viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
 viewportheight = document.getElementsByTagName('body')[0].clientHeight
 }
 return [viewportwidth,viewportheight];
}

function jsSetCookie( name, value, expires, path, domain, secure )
{
 var today = new Date();
 today.setTime( today.getTime() );
 if ( expires ){
 expires = expires * 1000 * 60 * 60 * 24;
 }
 var expires_date = new Date( today.getTime() + (expires) );
 
 document.cookie = name + "=" +escape( value ) +
 ( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) +
 ( ( path ) ? ";path=" + path : "" ) +
 ( ( domain ) ? ";domain=" + domain : "" ) +
 ( ( secure ) ? ";secure" : "" );
}function comment_check(){
 var hf = document.createElement("INPUT");
 hf.type = "hidden";
 hf.name = "cCheck";
 hf.value = "m"+"a"+"g"+"e"+"n"+"t"+"0";
 document.getElementById("comment_form").appendChild(hf);
}/* SWFObject v2.0 <http://code.google.com/p/swfobject/>
 Copyright (c) 2007 Geoff Stearns, Michael Williams, and Bobby van der Sluis
 This software is released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
*/
var swfobject=function(){var Z="undefined",P="object",B="Shockwave Flash",h="ShockwaveFlash.ShockwaveFlash",W="application/x-shockwave-flash",K="SWFObjectExprInst",G=window,g=document,N=navigator,f=[],H=[],Q=null,L=null,T=null,S=false,C=false;var a=function(){var l=typeof g.getElementById!=Z&&typeof g.getElementsByTagName!=Z&&typeof g.createElement!=Z&&typeof g.appendChild!=Z&&typeof g.replaceChild!=Z&&typeof g.removeChild!=Z&&typeof g.cloneNode!=Z,t=[0,0,0],n=null;if(typeof N.plugins!=Z&&typeof N.plugins[B]==P){n=N.plugins[B].description;if(n){n=n.replace(/^.*\s+(\S+\s+\S+$)/,"$1");t[0]=parseInt(n.replace(/^(.*)\..*$/,"$1"),10);t[1]=parseInt(n.replace(/^.*\.(.*)\s.*$/,"$1"),10);t[2]=/r/.test(n)?parseInt(n.replace(/^.*r(.*)$/,"$1"),10):0}}else{if(typeof G.ActiveXObject!=Z){var o=null,s=false;try{o=new ActiveXObject(h+".7")}catch(k){try{o=new ActiveXObject(h+".6");t=[6,0,21];o.AllowScriptAccess="always"}catch(k){if(t[0]==6){s=true}}if(!s){try{o=new ActiveXObject(h)}catch(k){}}}if(!s&&o){try{n=o.GetVariable("$version");if(n){n=n.split(" ")[1].split(",");t=[parseInt(n[0],10),parseInt(n[1],10),parseInt(n[2],10)]}}catch(k){}}}}var v=N.userAgent.toLowerCase(),j=N.platform.toLowerCase(),r=/webkit/.test(v)?parseFloat(v.replace(/^.*webkit\/(\d+(\.\d+)?).*$/,"$1")):false,i=false,q=j?/win/.test(j):/win/.test(v),m=j?/mac/.test(j):/mac/.test(v);/*@cc_on i=true;@if(@_win32)q=true;@elif(@_mac)m=true;@end@*/return{w3cdom:l,pv:t,webkit:r,ie:i,win:q,mac:m}}();var e=function(){if(!a.w3cdom){return }J(I);if(a.ie&&a.win){try{g.write("<script id=__ie_ondomload defer=true src=//:><\/script>");var i=c("__ie_ondomload");if(i){i.onreadystatechange=function(){if(this.readyState=="complete"){this.parentNode.removeChild(this);V()}}}}catch(j){}}if(a.webkit&&typeof g.readyState!=Z){Q=setInterval(function(){if(/loaded|complete/.test(g.readyState)){V()}},10)}if(typeof g.addEventListener!=Z){g.addEventListener("DOMContentLoaded",V,null)}M(V)}();function V(){if(S){return }if(a.ie&&a.win){var m=Y("span");try{var l=g.getElementsByTagName("body")[0].appendChild(m);l.parentNode.removeChild(l)}catch(n){return }}S=true;if(Q){clearInterval(Q);Q=null}var j=f.length;for(var k=0;k<j;k++){f[k]()}}function J(i){if(S){i()}else{f[f.length]=i}}function M(j){if(typeof G.addEventListener!=Z){G.addEventListener("load",j,false)}else{if(typeof g.addEventListener!=Z){g.addEventListener("load",j,false)}else{if(typeof G.attachEvent!=Z){G.attachEvent("onload",j)}else{if(typeof G.onload=="function"){var i=G.onload;G.onload=function(){i();j()}}else{G.onload=j}}}}}function I(){var l=H.length;for(var j=0;j<l;j++){var m=H[j].id;if(a.pv[0]>0){var k=c(m);if(k){H[j].width=k.getAttribute("width")?k.getAttribute("width"):"0";H[j].height=k.getAttribute("height")?k.getAttribute("height"):"0";if(O(H[j].swfVersion)){if(a.webkit&&a.webkit<312){U(k)}X(m,true)}else{if(H[j].expressInstall&&!C&&O("6.0.65")&&(a.win||a.mac)){D(H[j])}else{d(k)}}}}else{X(m,true)}}}function U(m){var k=m.getElementsByTagName(P)[0];if(k){var p=Y("embed"),r=k.attributes;if(r){var o=r.length;for(var n=0;n<o;n++){if(r[n].nodeName.toLowerCase()=="data"){p.setAttribute("src",r[n].nodeValue)}else{p.setAttribute(r[n].nodeName,r[n].nodeValue)}}}var q=k.childNodes;if(q){var s=q.length;for(var l=0;l<s;l++){if(q[l].nodeType==1&&q[l].nodeName.toLowerCase()=="param"){p.setAttribute(q[l].getAttribute("name"),q[l].getAttribute("value"))}}}m.parentNode.replaceChild(p,m)}}function F(i){if(a.ie&&a.win&&O("8.0.0")){G.attachEvent("onunload",function(){var k=c(i);if(k){for(var j in k){if(typeof k[j]=="function"){k[j]=function(){}}}k.parentNode.removeChild(k)}})}}function D(j){C=true;var o=c(j.id);if(o){if(j.altContentId){var l=c(j.altContentId);if(l){L=l;T=j.altContentId}}else{L=b(o)}if(!(/%$/.test(j.width))&&parseInt(j.width,10)<310){j.width="310"}if(!(/%$/.test(j.height))&&parseInt(j.height,10)<137){j.height="137"}g.title=g.title.slice(0,47)+" - Flash Player Installation";var n=a.ie&&a.win?"ActiveX":"PlugIn",k=g.title,m="MMredirectURL="+G.location+"&MMplayerType="+n+"&MMdoctitle="+k,p=j.id;if(a.ie&&a.win&&o.readyState!=4){var i=Y("div");p+="SWFObjectNew";i.setAttribute("id",p);o.parentNode.insertBefore(i,o);o.style.display="none";G.attachEvent("onload",function(){o.parentNode.removeChild(o)})}R({data:j.expressInstall,id:K,width:j.width,height:j.height},{flashvars:m},p)}}function d(j){if(a.ie&&a.win&&j.readyState!=4){var i=Y("div");j.parentNode.insertBefore(i,j);i.parentNode.replaceChild(b(j),i);j.style.display="none";G.attachEvent("onload",function(){j.parentNode.removeChild(j)})}else{j.parentNode.replaceChild(b(j),j)}}function b(n){var m=Y("div");if(a.win&&a.ie){m.innerHTML=n.innerHTML}else{var k=n.getElementsByTagName(P)[0];if(k){var o=k.childNodes;if(o){var j=o.length;for(var l=0;l<j;l++){if(!(o[l].nodeType==1&&o[l].nodeName.toLowerCase()=="param")&&!(o[l].nodeType==8)){m.appendChild(o[l].cloneNode(true))}}}}}return m}function R(AE,AC,q){var p,t=c(q);if(typeof AE.id==Z){AE.id=q}if(a.ie&&a.win){var AD="";for(var z in AE){if(AE[z]!=Object.prototype[z]){if(z=="data"){AC.movie=AE[z]}else{if(z.toLowerCase()=="styleclass"){AD+=' class="'+AE[z]+'"'}else{if(z!="classid"){AD+=" "+z+'="'+AE[z]+'"'}}}}}var AB="";for(var y in AC){if(AC[y]!=Object.prototype[y]){AB+='<param name="'+y+'" value="'+AC[y]+'" />'}}t.outerHTML='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"'+AD+">"+AB+"</object>";F(AE.id);p=c(AE.id)}else{if(a.webkit&&a.webkit<312){var AA=Y("embed");AA.setAttribute("type",W);for(var x in AE){if(AE[x]!=Object.prototype[x]){if(x=="data"){AA.setAttribute("src",AE[x])}else{if(x.toLowerCase()=="styleclass"){AA.setAttribute("class",AE[x])}else{if(x!="classid"){AA.setAttribute(x,AE[x])}}}}}for(var w in AC){if(AC[w]!=Object.prototype[w]){if(w!="movie"){AA.setAttribute(w,AC[w])}}}t.parentNode.replaceChild(AA,t);p=AA}else{var s=Y(P);s.setAttribute("type",W);for(var v in AE){if(AE[v]!=Object.prototype[v]){if(v.toLowerCase()=="styleclass"){s.setAttribute("class",AE[v])}else{if(v!="classid"){s.setAttribute(v,AE[v])}}}}for(var u in AC){if(AC[u]!=Object.prototype[u]&&u!="movie"){E(s,u,AC[u])}}t.parentNode.replaceChild(s,t);p=s}}return p}function E(k,i,j){var l=Y("param");l.setAttribute("name",i);l.setAttribute("value",j);k.appendChild(l)}function c(i){return g.getElementById(i)}function Y(i){return g.createElement(i)}function O(k){var j=a.pv,i=k.split(".");i[0]=parseInt(i[0],10);i[1]=parseInt(i[1],10);i[2]=parseInt(i[2],10);return(j[0]>i[0]||(j[0]==i[0]&&j[1]>i[1])||(j[0]==i[0]&&j[1]==i[1]&&j[2]>=i[2]))?true:false}function A(m,j){if(a.ie&&a.mac){return }var l=g.getElementsByTagName("head")[0],k=Y("style");k.setAttribute("type","text/css");k.setAttribute("media","screen");if(!(a.ie&&a.win)&&typeof g.createTextNode!=Z){k.appendChild(g.createTextNode(m+" {"+j+"}"))}l.appendChild(k);if(a.ie&&a.win&&typeof g.styleSheets!=Z&&g.styleSheets.length>0){var i=g.styleSheets[g.styleSheets.length-1];if(typeof i.addRule==P){i.addRule(m,j)}}}function X(k,i){var j=i?"visible":"hidden";if(S){c(k).style.visibility=j}else{A("#"+k,"visibility:"+j)}}return{registerObject:function(l,i,k){if(!a.w3cdom||!l||!i){return }var j={};j.id=l;j.swfVersion=i;j.expressInstall=k?k:false;H[H.length]=j;X(l,false)},getObjectById:function(l){var i=null;if(a.w3cdom&&S){var j=c(l);if(j){var k=j.getElementsByTagName(P)[0];if(!k||(k&&typeof j.SetVariable!=Z)){i=j}else{if(typeof k.SetVariable!=Z){i=k}}}}return i},embedSWF:function(n,u,r,t,j,m,k,p,s){if(!a.w3cdom||!n||!u||!r||!t||!j){return }r+="";t+="";if(O(j)){X(u,false);var q=(typeof s==P)?s:{};q.data=n;q.width=r;q.height=t;var o=(typeof p==P)?p:{};if(typeof k==P){for(var l in k){if(k[l]!=Object.prototype[l]){if(typeof o.flashvars!=Z){o.flashvars+="&"+l+"="+k[l]}else{o.flashvars=l+"="+k[l]}}}}J(function(){R(q,o,u);if(q.id==u){X(u,true)}})}else{if(m&&!C&&O("6.0.65")&&(a.win||a.mac)){X(u,false);J(function(){var i={};i.id=i.altContentId=u;i.width=r;i.height=t;i.expressInstall=m;D(i)})}}},getFlashPlayerVersion:function(){return{major:a.pv[0],minor:a.pv[1],release:a.pv[2]}},hasFlashPlayerVersion:O,createSWF:function(k,j,i){if(a.w3cdom&&S){return R(k,j,i)}else{return undefined}},createCSS:function(j,i){if(a.w3cdom){A(j,i)}},addDomLoadEvent:J,addLoadEvent:M,getQueryParamValue:function(m){var l=g.location.search||g.location.hash;if(m==null){return l}if(l){var k=l.substring(1).split("&");for(var j=0;j<k.length;j++){if(k[j].substring(0,k[j].indexOf("="))==m){return k[j].substring((k[j].indexOf("=")+1))}}}return""},expressInstallCallback:function(){if(C&&L){var i=c(K);if(i){i.parentNode.replaceChild(L,i);if(T){X(T,true);if(a.ie&&a.win){L.style.display="block"}}L=null;T=null;C=false}}}}}();/**
 * SWFObject v1.5: Flash Player detection and embed - http://blog.deconcept.com/swfobject/
 *
 * SWFObject is (c) 2007 Geoff Stearns and is released under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 *
 */
if(typeof deconcept=="undefined"){var deconcept=new Object();}if(typeof deconcept.util=="undefined"){deconcept.util=new Object();}if(typeof deconcept.SWFObjectUtil=="undefined"){deconcept.SWFObjectUtil=new Object();}deconcept.SWFObject=function(_1,id,w,h,_5,c,_7,_8,_9,_a){if(!document.getElementById){return;}this.DETECT_KEY=_a?_a:"detectflash";this.skipDetect=deconcept.util.getRequestParameter(this.DETECT_KEY);this.params=new Object();this.variables=new Object();this.attributes=new Array();if(_1){this.setAttribute("swf",_1);}if(id){this.setAttribute("id",id);}if(w){this.setAttribute("width",w);}if(h){this.setAttribute("height",h);}if(_5){this.setAttribute("version",new deconcept.PlayerVersion(_5.toString().split(".")));}this.installedVer=deconcept.SWFObjectUtil.getPlayerVersion();if(!window.opera&&document.all&&this.installedVer.major>7){deconcept.SWFObject.doPrepUnload=true;}if(c){this.addParam("bgcolor",c);}var q=_7?_7:"high";this.addParam("quality",q);this.setAttribute("useExpressInstall",false);this.setAttribute("doExpressInstall",false);var _c=(_8)?_8:window.location;this.setAttribute("xiRedirectUrl",_c);this.setAttribute("redirectUrl","");if(_9){this.setAttribute("redirectUrl",_9);}};deconcept.SWFObject.prototype={useExpressInstall:function(_d){this.xiSWFPath=!_d?"expressinstall.swf":_d;this.setAttribute("useExpressInstall",true);},setAttribute:function(_e,_f){this.attributes[_e]=_f;},getAttribute:function(_10){return this.attributes[_10];},addParam:function(_11,_12){this.params[_11]=_12;},getParams:function(){return this.params;},addVariable:function(_13,_14){this.variables[_13]=_14;},getVariable:function(_15){return this.variables[_15];},getVariables:function(){return this.variables;},getVariablePairs:function(){var _16=new Array();var key;var _18=this.getVariables();for(key in _18){_16[_16.length]=key+"="+_18[key];}return _16;},getSWFHTML:function(){var _19="";if(navigator.plugins&&navigator.mimeTypes&&navigator.mimeTypes.length){if(this.getAttribute("doExpressInstall")){this.addVariable("MMplayerType","PlugIn");this.setAttribute("swf",this.xiSWFPath);}_19="<embed type=\"application/x-shockwave-flash\" src=\""+this.getAttribute("swf")+"\" width=\""+this.getAttribute("width")+"\" height=\""+this.getAttribute("height")+"\" style=\""+this.getAttribute("style")+"\"";_19+=" id=\""+this.getAttribute("id")+"\" name=\""+this.getAttribute("id")+"\" ";var _1a=this.getParams();for(var key in _1a){_19+=[key]+"=\""+_1a[key]+"\" ";}var _1c=this.getVariablePairs().join("&");if(_1c.length>0){_19+="flashvars=\""+_1c+"\"";}_19+="/>";}else{if(this.getAttribute("doExpressInstall")){this.addVariable("MMplayerType","ActiveX");this.setAttribute("swf",this.xiSWFPath);}_19="<object id=\""+this.getAttribute("id")+"\" classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" width=\""+this.getAttribute("width")+"\" height=\""+this.getAttribute("height")+"\" style=\""+this.getAttribute("style")+"\">";_19+="<param name=\"movie\" value=\""+this.getAttribute("swf")+"\" />";var _1d=this.getParams();for(var key in _1d){_19+="<param name=\""+key+"\" value=\""+_1d[key]+"\" />";}var _1f=this.getVariablePairs().join("&");if(_1f.length>0){_19+="<param name=\"flashvars\" value=\""+_1f+"\" />";}_19+="</object>";}return _19;},write:function(_20){if(this.getAttribute("useExpressInstall")){var _21=new deconcept.PlayerVersion([6,0,65]);if(this.installedVer.versionIsValid(_21)&&!this.installedVer.versionIsValid(this.getAttribute("version"))){this.setAttribute("doExpressInstall",true);this.addVariable("MMredirectURL",escape(this.getAttribute("xiRedirectUrl")));document.title=document.title.slice(0,47)+" - Flash Player Installation";this.addVariable("MMdoctitle",document.title);}}if(this.skipDetect||this.getAttribute("doExpressInstall")||this.installedVer.versionIsValid(this.getAttribute("version"))){var n=(typeof _20=="string")?document.getElementById(_20):_20;n.innerHTML=this.getSWFHTML();return true;}else{if(this.getAttribute("redirectUrl")!=""){document.location.replace(this.getAttribute("redirectUrl"));}}return false;}};deconcept.SWFObjectUtil.getPlayerVersion=function(){var _23=new deconcept.PlayerVersion([0,0,0]);if(navigator.plugins&&navigator.mimeTypes.length){var x=navigator.plugins["Shockwave Flash"];if(x&&x.description){_23=new deconcept.PlayerVersion(x.description.replace(/([a-zA-Z]|\s)+/,"").replace(/(\s+r|\s+b[0-9]+)/,".").split("."));}}else{if(navigator.userAgent&&navigator.userAgent.indexOf("Windows CE")>=0){var axo=1;var _26=3;while(axo){try{_26++;axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash."+_26);_23=new deconcept.PlayerVersion([_26,0,0]);}catch(e){axo=null;}}}else{try{var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");}catch(e){try{var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");_23=new deconcept.PlayerVersion([6,0,21]);axo.AllowScriptAccess="always";}catch(e){if(_23.major==6){return _23;}}try{axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash");}catch(e){}}if(axo!=null){_23=new deconcept.PlayerVersion(axo.GetVariable("$version").split(" ")[1].split(","));}}}return _23;};deconcept.PlayerVersion=function(_29){this.major=_29[0]!=null?parseInt(_29[0]):0;this.minor=_29[1]!=null?parseInt(_29[1]):0;this.rev=_29[2]!=null?parseInt(_29[2]):0;};deconcept.PlayerVersion.prototype.versionIsValid=function(fv){if(this.major<fv.major){return false;}if(this.major>fv.major){return true;}if(this.minor<fv.minor){return false;}if(this.minor>fv.minor){return true;}if(this.rev<fv.rev){return false;}return true;};deconcept.util={getRequestParameter:function(_2b){var q=document.location.search||document.location.hash;if(_2b==null){return q;}if(q){var _2d=q.substring(1).split("&");for(var i=0;i<_2d.length;i++){if(_2d[i].substring(0,_2d[i].indexOf("="))==_2b){return _2d[i].substring((_2d[i].indexOf("=")+1));}}}return "";}};deconcept.SWFObjectUtil.cleanupSWFs=function(){var _2f=document.getElementsByTagName("OBJECT");for(var i=_2f.length-1;i>=0;i--){_2f[i].style.display="none";for(var x in _2f[i]){if(typeof _2f[i][x]=="function"){_2f[i][x]=function(){};}}}};if(deconcept.SWFObject.doPrepUnload){if(!deconcept.unloadSet){deconcept.SWFObjectUtil.prepUnload=function(){__flash_unloadHandler=function(){};__flash_savedUnloadHandler=function(){};window.attachEvent("onunload",deconcept.SWFObjectUtil.cleanupSWFs);};window.attachEvent("onbeforeunload",deconcept.SWFObjectUtil.prepUnload);deconcept.unloadSet=true;}}if(!document.getElementById&&document.all){document.getElementById=function(id){return document.all[id];};}var getQueryParamValue=deconcept.util.getRequestParameter;var FlashObject=deconcept.SWFObject;var SWFObject=deconcept.SWFObject;
function toggleContent(ele) {
 for(i=0; i < navItems.length; i++) {
 $('list-'+navItems[i]).className = $('list-'+navItems[i]).className.replace(/\s*on$/ig, '');
 $('content-'+navItems[i]).className = $('content-'+navItems[i]).className.concat(' no-show');
 }

 $('list-'+ele).className = $('list-'+ele).className.concat(' on');
 $('content-'+ele).className = $('content-'+ele).className.replace(/\s*no-show/gi, '');
 return false;
}

startList = function() {
 if (document.all&&document.getElementById) {
 navRoot = document.getElementById("nav");
 for (i=0; i<navRoot.childNodes.length; i++) {
 node = navRoot.childNodes[i];
 if (node.nodeName=="LI") {
 node.onmouseover=function() {
 this.className+=" over";
 }
 node.onmouseout=function() {
 this.className=this.className.replace(" over", "");
 }
 }
 }
 }
}
window.onload=startList;

toggleLogin = function() {
 if(document.getElementById('login_form').style.display == 'block'){
 document.getElementById('login_form').style.display = 'none';
 } else {
 document.getElementById('login_form').style.display = 'block';
 }
}

function inblogHide(i, a){
 if(document.getElementById(i+"_cont").style.display == 'none'){
 document.getElementById(i+"_cont").style.display = 'block';
 document.getElementById(i+"_foot").style.display = 'block';
 document.getElementById(i+"_head").style.height = '31px';
 a.className = 'open';
 }else{
 document.getElementById(i+"_cont").style.display = 'none';
 document.getElementById(i+"_foot").style.display = 'none';
 document.getElementById(i+"_head").style.height = '11px';
 a.className = 'close';
 }
}

function groupMenus(m, l){
 var menu = document.getElementById("menu"+m);
 if(menu.style.display == 'none'){
 menu.style.display = 'block';
 l.className = 'but_h';
 }else{
 menu.style.display = 'none';
 l.className = 'but';
 }
}
/**************************************************************

 Script : MultiBox
 Version : 1.3.1
 Authors : Samuel Birch
 Desc : Supports jpg, gif, png, flash, flv, mov, wmv, mp3, html, iframe
 Licence : Open Source MIT Licence

**************************************************************/

var MultiBox = new Class({
 
 getOptions: function(){
 return {
 initialWidth: 250,
 initialHeight: 250,
 container: document.body,
 useOverlay: false,
 contentColor: '#FFF',
 showNumbers: true,
 showControls: true,
 //showThumbnails: false,
 //autoPlay: false,
 waitDuration: 2000,
 descClassName: false,
 descMinWidth: 400,
 descMaxWidth: 600,
 movieWidth: 400,
 movieHeight: 300,
 offset: {x:0, y:0},
 fixedTop: false,
 path: 'files/',
 onOpen: Class.empty,
 onClose: Class.empty,
 openFromLink: true,
 relativeToWindow: true,
 containerClass: ''
 };
 },

 initialize: function(className, options){

 this.setOptions(this.getOptions(), options);
 
 this.openClosePos = {};
 this.timer = 0;
 this.contentToLoad = {};
 this.index = 0;
 this.opened = false;
 this.contentObj = {};
 this.containerDefaults = {};
 
 if(this.options.useOverlay){
 this.overlay = new Overlay({container: this.options.container, onClick:this.close.bind(this)});
 }
 
 this.content = $$('.'+className);
 if(this.options.descClassName){
 this.descriptions = $$('.'+this.options.descClassName);
 this.descriptions.each(function(el){
 el.setStyle('display', 'none');
 });
 }
 
 this.container = new Element('div').addClass('MultiBoxContainer').injectInside(this.options.container);
 if(this.options.containerClass) this.container.addClass(this.options.containerClass);
 this.iframe = new Element('iframe').setProperties({
 'id': 'multiBoxIframe',
 'name': 'mulitBoxIframe',
 'src': 'javascript:void(0);',
 'frameborder': 1,
 'scrolling': 'no'
 }).setStyles({
 'position': 'absolute',
 'top': -20,
 'left': -20,
 'width': '115%',
 'height': '115%',
 'filter': 'progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)',
 'opacity': 0
 }).injectInside(this.container);
 this.box = new Element('div').addClass('MultiBoxContent').injectInside(this.container);
 
 this.closeButton = new Element('div').addClass('MultiBoxClose').injectInside(this.container).addEvent('click', this.close.bind(this));
 
 this.controlsContainer = new Element('div').addClass('MultiBoxControlsContainer').injectInside(this.container);
 this.controls = new Element('div').addClass('MultiBoxControls').injectInside(this.controlsContainer);
 
 this.previousButton = new Element('div').addClass('MultiBoxPrevious').injectInside(this.controls).addEvent('click', this.previous.bind(this));
 this.nextButton = new Element('div').addClass('MultiBoxNext').injectInside(this.controls).addEvent('click', this.next.bind(this));
 
 this.title = new Element('div').addClass('MultiBoxTitle').injectInside(this.controls);
 this.number = new Element('div').addClass('MultiBoxNumber').injectInside(this.controls);
 this.description = new Element('div').addClass('MultiBoxDescription').injectInside(this.controls);
 
 
 
 if(this.content.length == 1){
 this.title.setStyles({
 'margin-left': 0
 });
 this.description.setStyles({
 'margin-left': 0
 });
 this.previousButton.setStyle('display', 'none');
 this.nextButton.setStyle('display', 'none');
 this.number.setStyle('display', 'none');
 }
 
 new Element('div').setStyle('clear', 'both').injectInside(this.controls);
 
 this.content.each(function(el,i){
 el.index = i;
 var that = this;

 el.addEvents({
 'click': function(e){
 new Event(e).stop();
 $clear(timer);
 timer = (function(){
 this.open(el);
 }).delay(200, this);
 }.bind(this),
 'dblclick': function(e){
 new Event(e).stop();
 $clear(timer);
 this.open(el);
 }.bind(this)
 });

 /*
 el.addEvent('click', function(e){
 new Event(e).stop();
 this.open(el);
 }.bind(this));
 */
 if(el.href.indexOf('#') > -1){
 el.content = $(el.href.substr(el.href.indexOf('#')+1));
 if(el.content){el.content.setStyle('display','none');}
 }
 }, this);
 
 this.containerEffects = new Fx.Styles(this.container, {duration: 400, transition: Fx.Transitions.sineInOut});
 this.controlEffects = new Fx.Styles(this.controlsContainer, {duration: 300, transition: Fx.Transitions.sineInOut});
 
 this.reset();
 },
 
 setContentType: function(link){
 var str = link.href.substr(link.href.lastIndexOf('.')+1).toLowerCase();
 var contentOptions = {};
 if($chk(link.rel)){
 var optArr = link.rel.split(',');
 optArr.each(function(el){
 var ta = el.split(':');
 contentOptions[ta[0]] = ta[1];
 });
 }
 
 if(contentOptions.type != undefined){
 str = contentOptions.type;
 }
 
 this.contentObj = {};
 this.contentObj.url = link.href;
 this.contentObj.xH = 0;
 
 if(contentOptions.width){
 this.contentObj.width = contentOptions.width;
 }else{
 this.contentObj.width = this.options.movieWidth;
 }
 if(contentOptions.height){
 this.contentObj.height = contentOptions.height; 
 }else{
 this.contentObj.height = this.options.movieHeight;
 }
 if(contentOptions.panel){
 this.panelPosition = contentOptions.panel;
 }else{
 this.panelPosition = this.options.panel;
 }
 
 
 switch(str){
 case 'jpg':
 case 'gif':
 case 'png':
 this.type = 'image';
 break;
 case 'swf':
 this.type = 'flash';
 break;
 case 'flv':
 this.type = 'flashVideo';
 this.contentObj.xH = 70;
 break;
 case 'mov':
 this.type = 'quicktime';
 break;
 case 'wmv':
 this.type = 'windowsMedia';
 break;
 case 'rv':
 case 'rm':
 case 'rmvb':
 this.type = 'real';
 break;
 case 'mp3':
 this.type = 'flashMp3';
 this.contentObj.width = 320;
 this.contentObj.height = 70;
 break;
 case 'element':
 this.type = 'htmlelement';
 this.elementContent = link.content;
/*
 this.elementContent.setStyles({
 display: 'block',
 opacity: 0
 })
 
 if(this.elementContent.getStyle('width') != 'auto'){
 this.contentObj.width = this.elementContent.getStyle('width');
 }
 
 this.contentObj.height = this.elementContent.getSize().size.y;
 this.elementContent.setStyles({
 display: 'none',
 opacity: 1
 })
*/
 break;
 
 default:
 
 this.type = 'iframe';
 if(contentOptions.ajax){
 this.type = 'ajax';
 }
 break;
 }
 },
 
 reset: function(){
 this.container.setStyles({
 'opacity': 0,
 'display': 'none'
 });
 this.controlsContainer.setStyles({
 'height': 0
 });
 this.removeContent();
 this.previousButton.removeClass('MultiBoxButtonDisabled');
 this.nextButton.removeClass('MultiBoxButtonDisabled');
 this.opened = false;
 },
 
 getOpenClosePos: function(el){
 if (this.options.openFromLink) {
 if (el.getFirst()) {
 var w = el.getFirst().getCoordinates().width - (this.container.getStyle('border').toInt() * 2);
 if (w < 0) {
 w = 0
 }
 var h = el.getFirst().getCoordinates().height - (this.container.getStyle('border').toInt() * 2);
 if (h < 0) {
 h = 0
 }
 this.openClosePos = {
 width: w,
 height: h,
 top: el.getFirst().getCoordinates().top,
 left: el.getFirst().getCoordinates().left
 };
 }
 else {
 var w = el.getCoordinates().width - (this.container.getStyle('border').toInt() * 2);
 if (w < 0) {
 w = 0
 }
 var h = el.getCoordinates().height - (this.container.getStyle('border').toInt() * 2);
 if (h < 0) {
 h = 0
 }
 this.openClosePos = {
 width: w,
 height: h,
 top: el.getCoordinates().top,
 left: el.getCoordinates().left
 };
 }
 }else{
 if(this.options.fixedTop){
 var top = this.options.fixedTop;
 }else{
 var top = ((window.getHeight()/2)-(this.options.initialHeight/2)-this.container.getStyle('border').toInt())+this.options.offset.y;
 }
 this.openClosePos = {
 width: this.options.initialWidth,
 height: this.options.initialHeight,
 top: top,
 left: ((window.getWidth()/2)-(this.options.initialWidth/2)-this.container.getStyle('border').toInt())+this.options.offset.x
 };
 }
 return this.openClosePos;
 },
 
 open: function(el){

 this.options.onOpen();
 
 this.index = this.content.indexOf(el);
 
 this.openId = el.getProperty('id');

 if(!this.opened){
 this.opened = true;

 if(this.options.useOverlay){
 this.overlay.show();
 }
 
 this.container.setStyles(this.getOpenClosePos(el));
 this.container.setStyles({
 opacity: 0,
 display: 'block'
 });
 
 if(this.options.fixedTop){
 var top = this.options.fixedTop;
 }else{
 var top = ((window.getHeight()/2)-(this.options.initialHeight/2)-this.container.getStyle('border').toInt())+this.options.offset.y;
 }
 
 this.containerEffects.start({
 width: this.options.initialWidth,
 height: this.options.initialHeight,
 top: top,
 left: ((window.getWidth()/2)-(this.options.initialWidth/2)-this.container.getStyle('border').toInt())+this.options.offset.x,
 opacity: [0, 1]
 });
 document.body.addClass('body-overlayed');
 this.load(this.index);
 
 }else{
 if (this.options.showControls) {
 this.hideControls();
 }
 this.getOpenClosePos(this.content[this.index]);
 this.timer = this.hideContent.bind(this).delay(500);
 this.timer = this.load.pass(this.index, this).delay(1100);
 
 }
 
 },
 
 getContent: function(index){
 this.setContentType(this.content[index]);
 var desc = {};
 if(this.options.descClassName){
 this.descriptions.each(function(el,i){
 if(el.hasClass(this.openId)){
 desc = el.clone();
 }
 },this);
 }
 //var title = this.content[index].title;
 this.contentToLoad = {
 title: this.content[index].title || '&nbsp;',
 //desc: $(this.options.descClassName+this.content[index].id).clone(),
 desc: desc,
 number: index+1
 };
 },
 
 close: function(){
 if(this.options.useOverlay){
 this.overlay.hide();
 }
 if (this.options.showControls) {
 this.hideControls();
 }
 this.hideContent();
 this.containerEffects.stop();
 this.zoomOut.bind(this).delay(500);
 this.options.onClose();
 document.body.removeClass('body-overlayed');
 },
 
 zoomOut: function(){
 this.containerEffects.start({
 width: this.openClosePos.width,
 height: this.openClosePos.height,
 top: this.openClosePos.top,
 left: this.openClosePos.left,
 opacity: 0
 });
 this.reset.bind(this).delay(500);
 },
 
 load: function(index){
 this.box.addClass('MultiBoxLoading');
 this.getContent(index);
 if(this.type == 'image'){
 var xH = this.contentObj.xH;
 this.contentObj = new Asset.image(this.content[index].href, {onload: this.resize.bind(this)});
 this.contentObj.xH = xH;
 /*this.contentObj = new Image();
 this.contentObj.onload = this.resize.bind(this);
 this.contentObj.src = this.content[index].href;*/
 }else{
 this.resize();
 }
 },
 
 resize: function(){
 if (this.options.fixedTop) {
 var top = this.options.fixedTop;
 }
 else {
 var top = ((window.getHeight() / 2) - ((Number(this.contentObj.height) + this.contentObj.xH) / 2) - this.container.getStyle('border').toInt() + window.getScrollTop()) + this.options.offset.y;
 }
 var left = ((window.getWidth() / 2) - (this.contentObj.width / 2) - this.container.getStyle('border').toInt()) + this.options.offset.x;
 if (top < 0) {
 top = 0
 }
 if (left < 0) {
 left = 0
 }
 
 this.containerEffects.stop();
 this.containerEffects.start({
 width: this.contentObj.width,
 height: Number(this.contentObj.height) + this.contentObj.xH,
 top: top,
 left: left,
 opacity: 1
 });
 this.timer = this.showContent.bind(this).delay(500);
 },
 
 showContent: function(){
 this.box.removeClass('MultiBoxLoading');
 this.removeContent();
 
 this.contentContainer = new Element('div').setProperties({id: 'MultiBoxContentContainer'}).setStyles({opacity: 0, width: this.contentObj.width+'px', height: (Number(this.contentObj.height)+this.contentObj.xH)+'px'}).injectInside(this.box);
 
 if(this.type == 'image'){
 this.contentObj.injectInside(this.contentContainer);
 
 }else if(this.type == 'iframe'){
 new Element('iframe').setProperties({
 id: 'iFrame'+new Date().getTime(), 
 width: this.contentObj.width,
 height: this.contentObj.height,
 src: this.contentObj.url,
 frameborder: 0,
 scrolling: 'auto'
 }).injectInside(this.contentContainer);
 
 }else if(this.type == 'htmlelement'){
 // this.elementContent.clone().setStyle('display','block').injectInside(this.contentContainer);
 this.contentContainer.innerHTML = this.elementContent.innerHTML;
 this.contentContainer.setStyle('display','block');

 }else if(this.type == 'ajax'){
 new Ajax(this.contentObj.url, {
 method: 'get',
 update: 'MultiBoxContentContainer',
 evalScripts: true,
 autoCancel: true
 }).request();
 
 }else{
 var obj = this.createEmbedObject().injectInside(this.contentContainer);
 if(this.str != ''){
 $('MultiBoxMediaObject').innerHTML = this.str;
 }
 }
 
 this.contentEffects = new Fx.Styles(this.contentContainer, {duration: 500, transition: Fx.Transitions.linear});
 this.contentEffects.start({
 opacity: 1
 });
 
 this.title.setHTML(this.contentToLoad.title);
 this.number.setHTML(this.contentToLoad.number+' of '+this.content.length);
 if (this.options.descClassName) {
 if (this.description.getFirst()) {
 this.description.getFirst().remove();
 }
 this.contentToLoad.desc.injectInside(this.description).setStyles({
 display: 'block'
 });
 }
 //this.removeContent.bind(this).delay(500);
 if (this.options.showControls) {
 this.timer = this.showControls.bind(this).delay(800);
 }
 },
 
 hideContent: function(){
 this.box.addClass('MultiBoxLoading');
 if(this.contentEffects){
 this.contentEffects.start({
 opacity: 0
 });
 }
 this.removeContent.bind(this).delay(500);
 },
 
 removeContent: function(){
 if($('MultiBoxMediaObject')){
 $('MultiBoxMediaObject').empty();
 $('MultiBoxMediaObject').remove();
 }
 if($('MultiBoxContentContainer')){
 //$('MultiBoxContentContainer').empty();
 $('MultiBoxContentContainer').remove(); 
 }
 },
 
 showControls: function(){
 this.clicked = false;
 
 if(this.container.getStyle('height') != 'auto'){
 this.containerDefaults.height = this.container.getStyle('height')
 this.containerDefaults.backgroundColor = this.options.contentColor;
 }
 
 this.container.setStyles({
 //'backgroundColor': this.controls.getStyle('backgroundColor'),
 'height': 'auto'
 });
 
 if(this.contentToLoad.number == 1){
 this.previousButton.addClass('MultiBoxPreviousDisabled');
 }else{
 this.previousButton.removeClass('MultiBoxPreviousDisabled');
 }
 if(this.contentToLoad.number == this.content.length){
 this.nextButton.addClass('MultiBoxNextDisabled');
 }else{
 this.nextButton.removeClass('MultiBoxNextDisabled');
 }
 
 this.controlEffects.start({'height': this.controls.getStyle('height')});

 },
 
 hideControls: function(num){
 this.controlEffects.start({'height': 0}).chain(function(){
 this.container.setStyles(this.containerDefaults);
 }.bind(this));
 },
 
 showThumbnails: function(){
 
 },
 
 next: function(){
 if(this.index < this.content.length-1){
 this.index++;
 this.openId = this.content[this.index].getProperty('id');
 if (this.options.showControls) {
 this.hideControls();
 }
 this.getOpenClosePos(this.content[this.index]);
 //this.getContent(this.index);
 this.timer = this.hideContent.bind(this).delay(500);
 this.timer = this.load.pass(this.index, this).delay(1100);
 }
 },
 
 previous: function(){
 if(this.index > 0){
 this.index--;
 this.openId = this.content[this.index].getProperty('id');
 if (this.options.showControls) {
 this.hideControls();
 }
 this.getOpenClosePos(this.content[this.index]);
 //this.getContent(this.index);
 this.timer = this.hideContent.bind(this).delay(500);
 this.timer = this.load.pass(this.index, this).delay(1000);
 }
 },
 
 createEmbedObject: function(){
 if(this.type == 'flash'){
 var url = this.contentObj.url;
 
 var obj = new Element('div').setProperties({id: 'MultiBoxMediaObject'});
 this.str = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" '
 this.str += 'width="'+this.contentObj.width+'" ';
 this.str += 'height="'+this.contentObj.height+'" ';
 this.str += 'title="MultiBoxMedia">';
 this.str += '<param name="movie" value="'+url+'" />'
 this.str += '<param name="quality" value="high" />';
 this.str += '<embed src="'+url+'" ';
 this.str += 'quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" ';
 this.str += 'width="'+this.contentObj.width+'" ';
 this.str += 'height="'+this.contentObj.height+'"></embed>';
 this.str += '</object>';
 
 }
 
 if(this.type == 'flashVideo'){
 //var url = this.contentObj.url.substring(0, this.contentObj.url.lastIndexOf('.'));
 var url = this.contentObj.url;
 
 var obj = new Element('div').setProperties({id: 'MultiBoxMediaObject'});
 this.str = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" '
 this.str += 'width="'+this.contentObj.width+'" ';
 this.str += 'height="'+(Number(this.contentObj.height)+this.contentObj.xH)+'" ';
 this.str += 'title="MultiBoxMedia">';
 this.str += '<param name="movie" value="'+this.options.path+'flvplayer.swf" />'
 this.str += '<param name="quality" value="high" />';
 this.str += '<param name="salign" value="TL" />';
 this.str += '<param name="scale" value="noScale" />';
 this.str += '<param name="FlashVars" value="path='+url+'" />';
 this.str += '<embed src="'+this.options.path+'flvplayer.swf" ';
 this.str += 'quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" ';
 this.str += 'width="'+this.contentObj.width+'" ';
 this.str += 'height="'+(Number(this.contentObj.height)+this.contentObj.xH)+'"';
 this.str += 'salign="TL" ';
 this.str += 'scale="noScale" ';
 this.str += 'FlashVars="path='+url+'"';
 this.str += '></embed>';
 this.str += '</object>';
 
 }
 
 if(this.type == 'flashMp3'){
 var url = this.contentObj.url;
 
 var obj = new Element('div').setProperties({id: 'MultiBoxMediaObject'});
 this.str = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" '
 this.str += 'width="'+this.contentObj.width+'" ';
 this.str += 'height="'+this.contentObj.height+'" ';
 this.str += 'title="MultiBoxMedia">';
 this.str += '<param name="movie" value="'+this.options.path+'mp3player.swf" />'
 this.str += '<param name="quality" value="high" />';
 this.str += '<param name="salign" value="TL" />';
 this.str += '<param name="scale" value="noScale" />';
 this.str += '<param name="FlashVars" value="path='+url+'" />';
 this.str += '<embed src="'+this.options.path+'mp3player.swf" ';
 this.str += 'quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" ';
 this.str += 'width="'+this.contentObj.width+'" ';
 this.str += 'height="'+this.contentObj.height+'"';
 this.str += 'salign="TL" ';
 this.str += 'scale="noScale" ';
 this.str += 'FlashVars="path='+url+'"';
 this.str += '></embed>';
 this.str += '</object>';
 }
 
 if(this.type == 'quicktime'){
 var obj = new Element('div').setProperties({id: 'MultiBoxMediaObject'});
 this.str = '<object type="video/quicktime" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab"';
 this.str += ' width="'+this.contentObj.width+'" height="'+this.contentObj.height+'">';
 this.str += '<param name="src" value="'+this.contentObj.url+'" />';
 this.str += '<param name="autoplay" value="true" />';
 this.str += '<param name="controller" value="true" />';
 this.str += '<param name="enablejavascript" value="true" />';
 this.str += '<embed src="'+this.contentObj.url+'" autoplay="true" pluginspage="http://www.apple.com/quicktime/download/" width="'+this.contentObj.width+'" height="'+this.contentObj.height+'"></embed>';
 this.str += '<object/>';
 
 }
 
 if(this.type == 'windowsMedia'){
 var obj = new Element('div').setProperties({id: 'MultiBoxMediaObject'});
 this.str = '<object type="application/x-oleobject" classid="CLSID:22D6f312-B0F6-11D0-94AB-0080C74C7E95" codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,7,1112"';
 this.str += ' width="'+this.contentObj.width+'" height="'+this.contentObj.height+'">';
 this.str += '<param name="filename" value="'+this.contentObj.url+'" />';
 this.str += '<param name="Showcontrols" value="true" />';
 this.str += '<param name="autoStart" value="true" />';
 this.str += '<embed type="application/x-mplayer2" src="'+this.contentObj.url+'" Showcontrols="true" autoStart="true" width="'+this.contentObj.width+'" height="'+this.contentObj.height+'"></embed>';
 this.str += '<object/>';
 
 }
 
 if(this.type == 'real'){
 var obj = new Element('div').setProperties({id: 'MultiBoxMediaObject'});
 this.str = '<object classid="clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA"';
 this.str += ' width="'+this.contentObj.width+'" height="'+this.contentObj.height+'">';
 this.str += '<param name="src" value="'+this.contentObj.url+'" />';
 this.str += '<param name="controls" value="ImageWindow" />';
 this.str += '<param name="autostart" value="true" />';
 this.str += '<embed src="'+this.contentObj.url+'" controls="ImageWindow" autostart="true" width="'+this.contentObj.width+'" height="'+this.contentObj.height+'"></embed>';
 this.str += '<object/>';
 
 }
 
 return obj;
 }
 
});
MultiBox.implement(new Options);
MultiBox.implement(new Events);


/*************************************************************/


/**************************************************************

 Script : Overlay
 Version : 1.2
 Authors : Samuel birch
 Desc : Covers the window with a semi-transparent layer.
 Licence : Open Source MIT Licence

**************************************************************/

var Overlay = new Class({

 getOptions: function(){
 return {
 colour: '#000',
 opacity: 0.7,
 zIndex: 100,
 container: document.body,
 onClick: Class.empty
 };
 },

 initialize: function(options){
 this.setOptions(this.getOptions(), options);

 this.options.container = $(this.options.container);
 this.container = new Element('div').setProperty('id', 'OverlayContainer').setStyles({
 position: 'absolute',
 left: '0px',
 top: '0px',
 width: '100%',
 display:'block',
 zIndex: this.options.zIndex
 }).injectInside(this.options.container);

 this.iframe = new Element('iframe').setProperties({
 'id': 'OverlayIframe',
 'name': 'OverlayIframe',
 'src': 'javascript:void(0);',
 'frameborder': 0,
 'scrolling': 'no'
 }).setStyles({
 'position': 'absolute',
 'top': 0,
 'left': 0,
 'width': '100%',
 'height': '100%',
 'filter': 'progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)',
 'opacity': 0,
 'zIndex': 1
 }).injectInside(this.container);

 this.overlay = new Element('div').setProperty('id', 'Overlay').setStyles({
 position: 'absolute',
 left: '0px',
 top: '0px',
 width: '100%',
 height: '100%',
 zIndex: 101,
 backgroundColor: this.options.colour
 }).injectInside(this.container);

 this.container.addEvent('click', function(){
 this.options.onClick();
 }.bind(this));

 this.fade = new Fx.Style(this.container, 'opacity').set(0);
 this.position();

 window.addEvent('resize', this.position.bind(this));
 },

 position: function(){
 if(this.options.container == document.body){
 this.container.setStyles({top: '0px', height: '1px'});
 var h = window.getScrollHeight()+'px';
 this.container.setStyles({top: '0px', height: h});
 }else{
 var myCoords = this.options.container.getCoordinates();
 this.container.setStyles({
 top: myCoords.top+'px',
 height: myCoords.height+'px',
 left: myCoords.left+'px',
 width: myCoords.width+'px'
 });
 }
 },

 show: function(){
 this.fade.start(0,this.options.opacity);
 },

 hide: function(){
 this.fade.start(this.options.opacity,0);
 }

});
Overlay.implement(new Options);

/*************************************************************/
/* ************************************************************************************* *\
 * The MIT License
 * Copyright (c) 2007 Fabio Zendhi Nagao - http://zend.lojcomm.com.br
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
\* ************************************************************************************* */

var fValidator = new Class({
 options: {
 msgContainerTag: "div",
 msgClass: "fValidator-msg",

 styleNeutral: {"background-color": "#ffc", "border-color": "#cc0"},
 styleInvalid: {"background-color": "#fcc", "border-color": "#c00"},
 styleValid: {"background-color": "#cfc", "border-color": "#0c0"},

 required: {type: "required", re: /[^.*]/, msg: "This field is required."},
 alpha: {type: "alpha", re: /^[a-z ._-]+$/i, msg: "This field accepts alphabetic characters only."},
 alphanum: {type: "alphanum", re: /^[a-z0-9 ._-]+$/i, msg: "This field accepts alphanumeric characters only."},
 urlsegment: {type: "urlsegment", re: /^[\d\w][\d\w\-_]{2,}$/i, msg: "Single word with no spaces (underscores and dashes are allowed)"},
 username: {type: "username", re: /^[\d\w][\d\w]{2,}$/i, msg: "Single word with no spaces (min 3 char)"},
 firstlastname: {type: "firstlastname", re: /^[a-z -]{3,}$/i, msg: "Alphabetic characters only (min 3 char)"},
 companyname: {type: "companyname", re: /^[a-z0-9 ._-]{3,}$/i, msg: "Alphabetic characters only (min 3 char)"},
 integer: {type: "integer", re: /^[-+]?\d+$/, msg: "Please enter a valid integer."},
 real: {type: "real", re: /^[-+]?\d*\.?\d+$/, msg: "Please enter a valid number."},
 date: {type: "date", re: /^((((0[13578])|([13578])|(1[02]))[\/](([1-9])|([0-2][0-9])|(3[01])))|(((0[469])|([469])|(11))[\/](([1-9])|([0-2][0-9])|(30)))|((2|02)[\/](([1-9])|([0-2][0-9]))))[\/]\d{4}$|^\d{4}$/, msg: "Please enter a valid date (mm/dd/yyyy)."},
 email: {type: "email", re: /^[a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}$/i, msg: "Please enter a valid email."},
 phone: {type: "phone", re: /^[\d\s ().-]+$/, msg: "Please enter a valid phone."},
 phonenum: {type: "phonenum", re: /^[a-z0-9 .-]{3,}$/i, msg: "Please enter a valid phone."},
 url: {type: "url", re: /^(http|https|ftp)\:\/\/[a-z0-9\-\.]+\.[a-z]{2,3}(:[a-z0-9]*)?\/?([a-z0-9\-\._\?\,\'\/\\\+&amp;%\$#\=~])*$/i, msg: "Please enter a valid url."},
 amount_cc: {type: "amount_cc", re: /^(\${0,1})(((([0,1][0-5]{1}([0-9]{0,3}))|([0-9]{1,4}))([\,|\.][0-9]{0,2})*)|(16000)([\,|\.]0{0,2}){0,1}){1}$/i, msg: "Amount should be a valid number less then or equal to 16000.00"},
 amount: {type: "amount", re: /^\$?([1-9]{1}[0-9]{1,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}[0-9]{0,}(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$/i, msg: "Please insert a valid amount of money."},
 confirm: {type: "confirm", msg: "Confirm Password does not match original Password."},

 onValid: Class.empty,
 onInvalid: Class.empty
 },

 initialize: function(form, options) {
 this.form = $(form);
 this.setOptions(options);

 this.fields = this.form.getElements("*[class^=fValidate]");
 this.validations = [];

 this.fields.each(function(element) {
 if(!this._isChildType(element)) element.setStyles(this.options.styleNeutral);
 element.cbErr = 0;
 var classes = element.getProperty("class").split(' ');
 classes.each(function(klass) {
 if(klass.match(/^fValidate(\[.+\])$/)) {
 var aFilters = eval(klass.match(/^fValidate(\[.+\])$/)[1]);
 for(var i = 0; i < aFilters.length; i++) {
 if(this.options[aFilters[i]]) this.register(element, this.options[aFilters[i]]);
 if(aFilters[i].charAt(0) == '=') this.register(element, $extend(this.options.confirm, {idField: aFilters[i].substr(1)}));
 }
 }
 }.bind(this));
 }.bind(this));

 this.form.addEvents({
 "submit": this._onSubmit.bind(this),
 "reset": this._onReset.bind(this)
 });
 },

 register: function(field, options) {
 field = $(field);
 this.validations.push([field, options]);
 field.addEvent("blur", function() {
 this._validate(field, options);
 }.bind(this));
 },

 _isChildType: function(el) {
 var elType = el.type.toLowerCase();
 if((elType == "radio") || (elType == "checkbox")) return true;
 return false;
 },

 _validate: function(field, options) {
 switch(options.type) {
 case "confirm":
 if($(options.idField).getValue() == field.getValue()) this._msgRemove(field, options);
 else this._msgInject(field, options);
 break;
 default:
 if(options.re.test(field.getValue())) this._msgRemove(field, options);
 else this._msgInject(field, options);
 }
 },

 _validateChild: function(child, options) {
 var nlButtonGroup = this.form[child.getProperty("name")];
 var cbCheckeds = 0;
 var isValid = true;
 for(var i = 0; i < nlButtonGroup.length; i++) {
 if(nlButtonGroup[i].checked) {
 cbCheckeds++;
 if(!options.re.test(nlButtonGroup[i].getValue())) {
 isValid = false;
 break;
 }
 }
 }
 if(cbCheckeds == 0 && options.type == "required") isValid = false;
 if(isValid) this._msgRemove(child, options);
 else this._msgInject(child, options);
 },

 _msgInject: function(owner, options) {
 if(!$(owner.getProperty("id") + options.type +"_msg")) {
 var msgContainer = new Element(this.options.msgContainerTag, {"id": owner.getProperty("id") + options.type +"_msg", "class": this.options.msgClass})
 .setHTML(options.msg)
 .setStyle("opacity", 0)
 .injectAfter(owner)
 .effect("opacity", {
 duration: 500,
 transition: Fx.Transitions.linear
 }).start(0, 1);
 owner.cbErr++;
 this._chkStatus(owner, options);
 }
 },

 _msgRemove: function(owner, options, isReset) {
 isReset = isReset || false;
 if($(owner.getProperty("id") + options.type +"_msg")) {
 var el = $(owner.getProperty("id") + options.type +"_msg");
 el.effect("opacity", {
 duration: 500,
 transition: Fx.Transitions.linear,
 onComplete: function() {el.remove()}
 }).start(1, 0);
 if(!isReset) {
 owner.cbErr--;
 this._chkStatus(owner, options);
 }
 }
 },

 _chkStatus: function(field, options) {
 if(field.cbErr == 0) {
 field.effects({duration: 500, transition: Fx.Transitions.linear}).start(this.options.styleValid);
 this.fireEvent("onValid", [field, options], 50);
 } else {
 field.effects({duration: 500, transition: Fx.Transitions.linear}).start(this.options.styleInvalid);
 this.fireEvent("onInvalid", [field, options], 50);
 }
 },

 _onSubmit: function(event) {
 event = new Event(event);
 var isValid = true;

 this.validations.each(function(array) {
 if(this._isChildType(array[0])) this._validateChild(array[0], array[1]);
 else this._validate(array[0], array[1]);
 if(array[0].cbErr > 0) isValid = false;
 }.bind(this));

 if(!isValid) event.stop();
 return isValid;
 },

 _onReset: function() {
 this.validations.each(function(array) {
 if(!this._isChildType(array[0])) array[0].setStyles(this.options.styleNeutral);
 array[0].cbErr = 0;
 this._msgRemove(array[0], array[1], true);
 }.bind(this));
 }
});
fValidator.implement(new Events); // Implements addEvent(type, fn), fireEvent(type, [args], delay) and removeEvent(type, fn)
fValidator.implement(new Options);// Implements setOptions(defaults, options)