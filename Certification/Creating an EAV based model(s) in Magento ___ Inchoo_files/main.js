$(document).ready(function() {
    $("#portfolio-preview").jcarousel({
        scroll: 1,
        wrap: 'circular'
    });	
    $('.project-box').hover(function(){
        $(".cover", this).stop().animate({
            top:'133px'
        },{
            queue:false,
            duration:360
        });
    }, function() {
        $(".cover", this).stop().animate({
            top:'0px'
        },{
            queue:false,
            duration:360
        });
    });

    $('.item').hover(function(){
        $(".view_rss", this).stop().animate({
            bottom:'0px'
        },{
            queue:false,
            duration:360
        });
    }, function() {
        $(".view_rss", this).stop().animate({
            bottom:'-92px'
        },{
            queue:false,
            duration:360
        });
    });


    $('ul#project-list li:nth-child(3n)').addClass('plain');
    $('ul#the-inchooers li:nth-child(10n)').addClass('plain');
    $("a.open-close-form").click(function() {
        $("#newsletter-subscription").fadeToggle("fast","linear");
    });
    $("ol.cf-ol li label").inFieldLabels();
    $("label.infield").inFieldLabels();
    $("input").attr("autocomplete","off");

    $('#cforms4form').attr('id', 'cformsform');



});

$(function(){
    $('#slides').slides({
        preload: true,
        play: 5000,
        pause: 2500,
        hoverPause: true
    });
});

//$(".toTop").click(function() {
//  $("html").animate({ scrollTop: 0 }, "slow");
//  return false;
//});

$('body.page-template-contact-template-php label').addClass('infield');
$('body.page-template-get-a-quote-template-php label').addClass('infield');
$('input#sendbutton').addClass('bt send-over');
$("div#twitter-selector ul").tabs(".twitter > ul");

$(document).ready(function()
{
    // jQuery LightBox
    $("div.entry-content a[href$='.png']").lightBox();
    $("div.entry-content a[href$='.jpg']").lightBox();
    $("div.entry-content a[href$='.gif']").lightBox();
});


     var g1;

     window.onload = function(){
       var g1 = new JustGage({
         id: "g1",
         value: 13,
         min: 0,
         max: 13,
         showMinMax: false,
         title: "We have 13 Certified Magento Developers",
           titleFontColor  : "#fff",
           valueFontColor   : "#fff",
           gaugeColor: "#fff",
           levelColors: ["#F57B20"],
           startAnimationTime: 2000,
                   startAnimationType: ">",
                   refreshAnimationTime: 1000,
                   refreshAnimationType: "bounce"
       });
     };

